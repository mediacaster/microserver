# MediaCaster - µServer
MediaCaster enables users with an Android device to add audio-visual content such as YouTube videos to a shared playlist that is played by a server.

This repository contains the embedded server application, currently under development, and that will allow to run an audio-only MediaCaster server on a [STM32H747I-DISCO](https://www.st.com/en/evaluation-tools/stm32h747i-disco.html) board with a dual-core [STM32H747XI](https://www.st.com/en/microcontrollers-microprocessors/stm32h747xi.html) microcontroller. The board is connected through UART to an [ESP32-DevKitC-32E](https://www.espressif.com/en/products/devkits/esp32-devkitc) to enable Wi-Fi and Bluetooth communication.

MediaCaster is a personal project, started in 2018, that was initially composed of an Android client and desktop server application, which can respectively be found on the [Client](https://gitlab.com/mediacaster/client) and [Server](https://gitlab.com/mediacaster/server) repositories. The development of the embedded server application began in July 2020 with a view to enhance my skills in embedded systems and my knowledge of real-time kernels. With this in mind, every part of the project, from real-time kernel to MP3 decoder, is developed from scratch. 

## Technologies
* C
* ARM Cortex-M4/M7
* STM32H747
* ESP32
* WM8994 audio codec
* Wi-Fi
* Bluetooth
* UART
* I2C
* I2S

## Development progress
- [ ] Cortex-M7 application
    - [x] Audio player
        - [x] I2C driver
        - [x] WM8994 audio codec driver
        - [x] Serial Audio Interface driver (I2S)
    - [ ] Audio decoders
        - [x] WAV
        - [ ] MP3
        - [ ] AAC
        - [ ] Vorbis
        - [ ] Opus
    - [ ] Main application
        - [x] System clock configuration
        - [x] MPU configuration
        - [x] Cortex-M4 communication
        - [x] Audio decoding
        - [x] Audio playing
        - [ ] Decoding error handling
        - [ ] Standby mode
- [ ] Cortex-M4 application
    - [x] Real-time kernel
    - [ ] Wi-Fi/Bluetooth driver
        - [ ] UART driver
        - [ ] ESP32 driver
    - [ ] Main application
        - [ ] Remote configuration through Bluetooth
        - [ ] Client request handling
        - [ ] Playlist management
        - [ ] Audio data downloading
        - [ ] Cortex-M7 communication
        - [ ] Standby mode
- [x] Inter-core communication interface

## Implementation details
It has been chosen to organize the project as follows: the high-performance Cortex-M7 core is dedicated to audio decoding and communication with WM8994 codec for audio playing, while the Cortex-M4 core is responsible for handling requests from clients through the ESP32 device, in addition to playlist management. Inter-core communication is enabled thanks to shared memory and inter-core notifications using events. That architecture allows to take full advantage of the Cortex-M7 performance capabilities for audio decoding, the Cortex-M4 core, WM8994 codec and DMAs being used to release it from other tasks.

The Cortex-M4 core sends the received encoded audio data in chunks to Cortex-M7 core using DMA. The data is copied to Cortex-M7 tightly coupled memory to speed up the decoding process. Those chunks are decoded one after the other by the Cortex-M7 core and decoded data is transferred using DMA to a large buffer, the content of which is transmitted to the WM8994 codec that will eventually sends the analog audio through the board headphone jack.

Communication with clients for playlist management and playback control is performed using UDP since the ESP32 device can only handle a very small number of simultaneous TCP connections. Only audio data is transmitted using TCP, in particular to be able to take advantage of the TCP flow control.

## Authors
* ALTENBACH Thomas - @taltenba
