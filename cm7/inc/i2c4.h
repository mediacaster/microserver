#ifndef I2C4_H
#define I2C4_H

#include <stddef.h>
#include <stdint.h>

/**
 * @brief Initializes I2C4.
 *
 * This function assumes the I2C4 kernel clock frequency is 100 MHz.
 */
void i2c4_init();

// The device 7-bit address must be shifted to the left
uint32_t i2c4_write_reg16(uint32_t dev_addr, uint32_t reg_addr, uint32_t reg_val);

// The device 7-bit address must be shifted to the left
uint32_t i2c4_write_reg16_many(uint32_t dev_addr, uint32_t reg_addr, uint32_t reg_val,
                               size_t reg_count);

// The device 7-bit address must be shifted to the left
int32_t i2c4_read_reg16(uint32_t dev_addr, uint32_t reg_addr);

#endif /* I2C4_H */
