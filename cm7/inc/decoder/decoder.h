#ifndef DECODER_H
#define DECODER_H

#include <stddef.h>
#include <stdint.h>
#include "audiofmt.h"
#include "audioinfo.h"
#include "decctx.h"

typedef uint32_t (*decoder_func_t)(decctx_t*, void*, size_t*);

typedef struct {
    decoder_func_t decoding_func;
    decctx_t ctx;
} decoder_t;

void decoder_init(decoder_t* decoder, audiofmt_t fmt);

void decoder_set_src(decoder_t* decoder, void const* encoded_data, size_t size);

uint32_t decoder_decode(decoder_t* decoder, void* decoded_data, size_t* max_size);

__attribute__((always_inline))
static inline uint32_t decoder_has_data(decoder_t const* decoder)
{
    return decoder->ctx.src_size != 0U;
}

__attribute__((always_inline))
static inline audioinfo_t const* decoder_get_audio_info(decoder_t const* decoder)
{
    return &decoder->ctx.audio_info;
}

#endif /* DECODER_H */
