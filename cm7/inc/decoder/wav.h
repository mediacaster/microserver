#ifndef WAV_H
#define WAV_H

#include <stddef.h>
#include <stdint.h>
#include "decctx.h"

uint32_t wav_decode(decctx_t* ctx, void* dest, size_t* dest_size);

#endif /* WAV_H */
