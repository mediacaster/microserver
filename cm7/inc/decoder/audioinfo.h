#ifndef AUDIOINFO_H
#define AUDIOINFO_H

typedef struct {
    uint32_t sample_rate;
    uint16_t resolution;
    uint16_t stereo;
} audioinfo_t;

#endif /* AUDIOINFO_H */
