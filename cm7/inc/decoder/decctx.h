#ifndef DECCTX_H
#define DECCTX_H

#include <stddef.h>
#include <stdint.h>
#include "audioinfo.h"

// TODO: choose right value
#define DECCTX_READ_BUFFER_SIZE 36U

typedef struct {
    uint32_t chunk_data_size;
    uint32_t chunk_id;
    uint8_t chunk_padding;
    uint8_t riff_header_parsed;
    uint8_t decoding_chunk;
} decctx_wav_t;

typedef struct {
    uint8_t const* src;
    size_t src_size;
    size_t buffer_size;
    audioinfo_t audio_info;
    uint8_t buffer[DECCTX_READ_BUFFER_SIZE];

    union {
        decctx_wav_t wav;
        // TODO: add others
    };
} decctx_t;

void decctx_init(decctx_t* ctx);

void const* decctx_read(decctx_t* ctx, size_t size);

void const* decctx_read_max(decctx_t* ctx, size_t* max_size);

uint32_t decctx_skip(decctx_t* ctx, size_t size);

#endif /* DECCTX_H */
