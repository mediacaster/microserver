#ifndef CODEC_H
#define CODEC_H

#include <stdint.h>

#define CODEC_MAX_VOLUME 0xC0U

typedef enum {
    CODEC_SAMPLE_RATE_8K,
    CODEC_SAMPLE_RATE_12K,
    CODEC_SAMPLE_RATE_16K,
    CODEC_SAMPLE_RATE_24K,
    CODEC_SAMPLE_RATE_32K,
    CODEC_SAMPLE_RATE_48K,
    CODEC_SAMPLE_RATE_96K,
    CODEC_SAMPLE_RATE_11_025K,
    CODEC_SAMPLE_RATE_22_05K,
    CODEC_SAMPLE_RATE_44_1K,
    CODEC_SAMPLE_RATE_88_2K,
    CODEC_SAMPLE_RATE_INVALID
} codec_sample_rate_t;

typedef enum {
    CODEC_SAMPLE_RATE_GROUP_8K,
    CODEC_SAMPLE_RATE_GROUP_11_025K
} codec_sample_rate_group_t;

typedef enum {
    CODEC_RESOLUTION_8,
    CODEC_RESOLUTION_16,
    CODEC_RESOLUTION_20,
    CODEC_RESOLUTION_24,
    CODEC_RESOLUTION_32,
    CODEC_RESOLUTION_INVALID
} codec_resolution_t;

/**
 * @brief Initializes the audio codec.
 *
 * By default codec is configured for 24-bit stereo audio data with 48 kHz
 * sample rate.
 *
 * When this function exits the codec is initially disabled.
 *
 * The codec is initially muted, it can be un-muted with the codec_set_volume()
 * function.
 */
void codec_init(void);

/**
 * @brief Enables the codec.
 *
 * The SAI1_MCLKA clock, connected to codec MCLK1 clock, must be enabled when
 * calling this function. The clock frequency must be set according to the
 * currently configured audio sample rate.
 *
 * The first time this function is called a cold startup sequence is executed,
 * taking around 300 ms to run. Otherwise a warm startup sequence is executed,
 * that should take less than 10 ms to run.
 *
 * This function must not be called in an exception handler with a priority
 * greater than or equal to 0 (meaning a priority value lower than or equal to
 * 0) as this could lead to a deadlock.
 *
 * If the codec is already enabled this function returns immediately. Otherwise
 * when this function exits the codec is initially in idle mode,
 * codec_exit_idle() must therefore be called before sending audio data to the
 * codec.
 */
void codec_enable(void);

/**
 * @brief Disables the codec.
 *
 * The codec clock is disabled, reducing power consumption. The latter can be
 * further reduced by powering the codec down in case it is not in use for an
 * extended period of time.
 *
 * After calling this function the SAI1_MCLKA clock, connected to codec MCLK1
 * clock can be safely disabled.
 *
 * If the codec is already disabled this function returns immediately.
 */
void codec_disable(void);

/**
 * @brief Powers on the audio codec on after it has been powered off.
 *
 * This function can only be called to power the codec after it has been powered
 * off by calling codec_power_off().
 *
 * The SAI1_MCLKA clock, connected to codec MCLK1 clock, must be enabled when
 * calling this function. The clock frequency must be set according to the
 * currently configured audio sample rate.
 *
 * When this function exits the codec is in idle mode, codec_exit_idle() must
 * therefore be called before sending audio data to the codec.
 *
 * The powering on sequence should take around 45 ms to run.
 */
void codec_power_on(void);

/**
 * @brief Powers off the audio codec.
 *
 * This function can be used to achieve maximum reduction of the codec power
 * consumption.
 *
 * After calling this function the codec must be powered on using the
 * codec_power_on() function before any subsequent use.
 *
 * After calling this function the SAI1_MCLKA clock, connected to codec MCLK1
 * clock can be safely disabled.
 *
 * This function must not be called when codec is disabled.
 *
 * The powering off sequence should take around 40 ms to run.
 */
void codec_power_off(void);

/**
 * @brief Make codec enter low-power idle mode.
 *
 * When in idle mode the codec cannot be used to play audio.
 *
 * If the codec is not in use for an extended period of time, it can be
 * disabled or even powered down to reduce further the power consumption.
 */
void codec_enter_idle(void);

/**
 * @brief Makes codec exit low-power idle mode.
 */
void codec_exit_idle(void);

/**
 * @brief Set audio codec sample rate.
 * @param sample_rate The sample rate, in Hz.
 *
 * The SAI1_MCLKA clock frequency must be adjusted according to the configured
 * sample rate as follow:
 *    - For 8 kHz, 11.025 kHz and 12 kHz sample rates, a 512*sample_rate
 *      SAI1_MCLKA clock must be used.
 *    - For 16 kHz, 22.05 kHz, 24 kHz, 32 kHz, 44.1 kHz, 48 kHz, 88.2 kHz and
 *      96 kHz sample rates a 256*sample_rate SAI1_MCLKA clock must be used.
 *
 * This function must be called with codec disabled.
 */
void codec_set_sample_rate(codec_sample_rate_t sample_rate);

/**
 * @brief Set audio codec resolution.
 * @param resolution The resolution in bits.
 */
void codec_set_resolution(codec_resolution_t resolution);

/**
 * @brief Set the codec playback volume.
 * @param volume Volume value from 0 to CODEC_MAX_VOLUME, inclusive.
 *
 * If the specified volume is zero, the playback is soft-muted.
 */
void codec_set_volume(uint32_t volume);

/**
 * @brief Returns the enumeration value corresponding to a given audio sample
 *        rate.
 * @param hz The sample rate in Hz.
 * @return The enumeration value corresponding to the specified sample rate if
 *         the codec is able to handle such sample rate,
 *         CODEC_SAMPLE_RATE_INVALID otherwise.
 */
codec_sample_rate_t codec_sample_rate_from_hz(uint32_t hz);

/**
 * @brief Returns the sample rate group to which a given sample rate belongs.
 * @param rate The sample rate.
 * @return The sample rate group to which the specified sample rate belongs.
 */
codec_sample_rate_group_t codec_sample_rate_get_group(codec_sample_rate_t rate);

/**
 * @brief Returns the enumeration value corresponding to a given audio
 *        resolution.
 * @param bits The resolution in bits.
 * @return The enumeration value corresponding to the specified audio
 *         resolution if the codec is able to handle such resolution,
 *         CODEC_RESOLUTION_INVALID otherwise.
 */
codec_resolution_t codec_resolution_from_bits(uint32_t bits);

#endif /* CODEC_H */
