#ifndef AUDIO_H
#define AUDIO_H

#include <stddef.h>
#include <stdint.h>
#include <stm32h7xx.h>
#include "codec.h"

/**
 * @brief Initializes audio peripherals.
 *
 * By default audio peripherals are configured for 24-bit stereo audio with
 * 48 kHz sample rate.
 *
 * The playback is initially muted and can be unmuted using the
 * audio_set_volume() function.
 *
 * This function assumes HSE oscillator is ready and is selected as PLL source.
 * PLL2 is used for audio peripheral clocking.
 */
void audio_init(void);

/**
 * @brief Puts audio peripherals in standby mode.
 *
 * The audio codec is shut down and SAI module is disabled to reduce power
 * consumption. The codec shutdown sequence takes around 40 ms to run.
 *
 * This function must only be called when playback is stopped.
 *
 * After calling this function, audio_exit_standby() must be called before using
 * any other routine of this file.
 */
void audio_enter_standby(void);

/**
 * @brief Makes audio peripherals exit standby mode.
 *
 * The audio codec is restarted and SAI module is re-enabled. The codec restart
 * sequence takes around 45 ms to run.
 */
void audio_exit_standby(void);

/**
 * @brief Set audio data properties.
 * @param sample_rate The audio sample rate in Hz.
 * @param resolution The audio resolution in bits.
 * @param mono Set to 1 for mono audio, 0 for stereo.
 * @return 0 on success, another value if the specified sample rate or
 *         resolution is not supported.
 *
 * This function must only be called when playback is stopped.
 */
uint32_t audio_config(uint32_t sample_rate, uint32_t resolution, uint32_t mono);

/**
 * @brief Starts or continues audio playback.
 * @param data The audio data to play, in PCM format.
 * @param size The size of the audio data, in bytes (at most 65535 bytes).
 *
 * The audio data is transfered to the audio codec asynchronously using DMA.
 * When the transfer has been completed the audio_transfer_complete() callback
 * is invoked.
 *
 * This function must not be called when a transfer is ongoing. It is possible
 * to check if a transfer is currently ongoing by calling the audio_is_busy()
 * function.
 *
 * This function can safely be called from an exception handler to continue
 * playback, however if the playback is currently stopped (after audio_stop()
 * has been invoked or the first time this function is called) this function
 * must not be invoked from an exception handler with a priority greater than
 * or equal to 0 (meaning a priority value lower than or equal to 0) as that
 * could lead to a deadlock.
 *
 * The audio data must contains an integer number of samples.
 */
void audio_play(void const* data, size_t size);

/**
 * @brief Pauses the audio playback.
 *
 * Calling this function disables in fact the DMA stream used to transfer
 * audio data to the codec, but that stream must not be used during the whole
 * time the playback is paused.
 *
 * This function must only be called when audio playback is ongoing.
 */
void audio_pause(void);

/**
 * @brief Resumes the audio playback.
 *
 * This function must only be called when audio playback is paused.
 */
void audio_resume(void);

/**
 * @brief Stops the audio playback.
 *
 * If an audio transfer is ongoing the transfer is stopped and all buffers are
 * flushed. A new transfer can therefore be safely started using the
 * audio_play() function after this function returns. The
 * audio_transfer_complete() callback is not called when transfer is stopped.
 */
void audio_stop(void);

/**
 * @brief Indicates whether audio data is currently being transfered to the
 *        audio codec.
 * @return 1 if audio is currently being transfered to the codec, 0 otherwise.
 *
 * The audio_play() function can only be called when this function returns 0.
 */
uint32_t audio_is_busy(void);

/**
 * @brief Indicates whether the playback is stopped.
 * @return 1 if the playback is stopped, 0 otherwise.
 */
uint32_t audio_is_stopped(void);

/**
 * @brief Set playback volume.
 * @param volume Volume value in percentage from 0% (mute) to 100% (max volume).
 *
 * If the specified volume is zero, the playback is soft-muted.
 */
__STATIC_FORCEINLINE void audio_set_volume(uint32_t volume)
{
    codec_set_volume((CODEC_MAX_VOLUME * volume) / 100U);
}

/**
 * @brief Callback called each time an audio transfer has been completed.
 *
 * This function can be implemented by the user if needed.
 */
void audio_transfer_complete(void);

#endif /* AUDIO_H */
