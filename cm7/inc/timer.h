#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>

// This function must not be called from IRQ with priority higher than or equal
// to the timer IRQ priority, or with timer IRQ priority level masked.
// The priority level is let by default by this function and should therefore
// be 0 if not modified by the user.
void timer_delay(uint32_t ms);

#endif /* TIMER_H */
