#ifndef CORE_H
#define CORE_H

void core_init(void);

void core_poll(void);

#endif /* CORE_H */
