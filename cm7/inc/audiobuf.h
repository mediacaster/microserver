#ifndef AUDIOBUF_H
#define AUDIOBUF_H

#include <stdint.h>

typedef struct {
    void* data;
    uint16_t size;
} audiobuf_block_t;

// uint16_t have been used for storing the sizes since it is sufficient for the
// current buffer uses and allow the structure to fit in a single cache line of
// CPU1 (32 bytes).
typedef struct {
    audiobuf_block_t head_block;
    uint16_t added_blocks;
    audiobuf_block_t tail_block;
    uint16_t removed_blocks;
    uint16_t block_size;
    uint16_t max_block_count;
    uint8_t* data;
    uint32_t volatile flushing;
} audiobuf_t;

void audiobuf_init(audiobuf_t* buf, void* data, uint16_t block_size, uint16_t max_block_count);

audiobuf_block_t const* audiobuf_request_put(audiobuf_t* buf);

void audiobuf_commit_put(audiobuf_t* buf, uint16_t size);

audiobuf_block_t const* audiobuf_peek(audiobuf_t* buf);

void audiobuf_pop(audiobuf_t* buf);

void audiobuf_trigger_flush(audiobuf_t* buf);

uint32_t audiobuf_is_flushing(audiobuf_t const* buf);

void audiobuf_clear(audiobuf_t* buf);

#endif /* AUDIOBUF_H */
