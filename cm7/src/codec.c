#include <stm32h7xx.h>
#include <timer.h>
#include "codec.h"
#include "i2c4.h"
#include "asrt.h"

#define CODEC_I2C_ADDR 0x34U

static void wait_end_of_sequence(uint32_t delay_ms);

/**
 * @brief Set to 1 iff codec is currently enabled.
 */
static uint32_t g_enabled = 0U;

void codec_init(void)
{
    i2c4_init();

    /* Reset codec */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0000U, 0x0000U);

    /*
     * WM8994 rev C errata work-arounds.
     * Tweak DC servo and DSP configuration for improved performance.
     */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0102U, 0x0003U);
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0056U, 0x0003U);
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0817U, 0x0000U);
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0102U, 0x0000U);

    /* Enable dynamic charge pump power control */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0051U, 0x0001U);

    /* Enable high performance DAC oversample rate */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0620U, 0x0003U);
    /* Enable DSP and AIF1 processing clock */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0208U, 0x000AU);

    /* Enable DAC1L/R and AIF1DAC1L/R input paths */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0005U, 0x0303U);
    /* Enable paths from AIF1L/R to DAC1L/R */
    i2c4_write_reg16_many(CODEC_I2C_ADDR, 0x0601U, 0x0001U, 2U);
    /* Enable paths from DAC1L/R to HPOUT1L/R */
    i2c4_write_reg16_many(CODEC_I2C_ADDR, 0x002DU, 0x0100U, 2U);
}

void codec_enable(void)
{
    /* Set to 1 when codec cold startup sequence has been executed */
    static uint32_t cold_start_performed = 0;

    /* Ensure codec is currently disabled */
    if (g_enabled) {
        return; /* Otherwise return immediately */
    }
    /* Enable AIF1CLK = MCLK1 clock */
    uint32_t aif1clk = i2c4_read_reg16(CODEC_I2C_ADDR, 0x0200U);
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0200U, aif1clk | 0x0001U);

    if (__builtin_expect(!cold_start_performed, 0U)) {
        /* Initiate headphone cold start-up sequence */
        i2c4_write_reg16(CODEC_I2C_ADDR, 0x0110U, 0x8100U);
        /* Wait until the sequence is over (should take around 300 ms) */
        wait_end_of_sequence(300U);

        /* Put VMID in low-power standby mode */
        i2c4_write_reg16(CODEC_I2C_ADDR, 0x0001U, 0x0305U);

        cold_start_performed = 1U;
    } else {
        /* Re-enable charge pump, DC servo and headphone output drivers */
        i2c4_write_reg16(CODEC_I2C_ADDR, 0x004CU, 0x8000U);
        timer_delay(6U);
        i2c4_write_reg16(CODEC_I2C_ADDR, 0x0001U, 0x0303U);
        timer_delay(1U);
        i2c4_write_reg16(CODEC_I2C_ADDR, 0x0060U, 0x0022U);
        i2c4_write_reg16(CODEC_I2C_ADDR, 0x0054U, 0x0003U);
        i2c4_write_reg16(CODEC_I2C_ADDR, 0x0060U, 0x00EEU);
    }

    g_enabled = 1U;
}

void codec_disable(void)
{
    /* Ensure codec is currently enabled */
    if (!g_enabled) {
        return; /* Otherwise return immediately */
    }

    /* Soft-mute AIF1DAC1 */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0420U, 0x0200U);

    /*
     * Charge pump and DC servo, on which headphone output drivers depend, must
     * be disabled since they need clocking.
     */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0060U, 0x0000U);
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0054U, 0x0000U);
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0001U, 0x0005U);
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x004CU, 0x0000U);

    /* Disable AIF1CLK */
    uint32_t aif1clk = i2c4_read_reg16(CODEC_I2C_ADDR, 0x0200U);
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0200U, aif1clk & ~0x0001U);

    g_enabled = 0U;
}

void codec_power_on()
{
    ASSERT(!g_enabled);

    /* Enable AIF1CLK = MCLK1 clock */
    uint32_t aif1clk = i2c4_read_reg16(CODEC_I2C_ADDR, 0x0200U);
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0200U, aif1clk | 0x0001U);

    /* Initiate headphone warm start-up sequence */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0110U, 0x8108U);
    /* Wait until the sequence is over (should take around 45 ms) */
    wait_end_of_sequence(45U);

    /* Put VMID in low-power standby mode */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0001U, 0x0305U);
}

void codec_power_off()
{
    ASSERT(g_enabled);

    /* Soft-mute AIF1DAC1 */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0420U, 0x0200U);

    /* Initiate headphone fast shut-down sequence */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0110U, 0x8122U);
    /* Wait until sequence is over, shut-down should take around 40 ms to run */
    wait_end_of_sequence(40U);

    /* Disable AIF1CLK */
    uint32_t aif1clk = i2c4_read_reg16(CODEC_I2C_ADDR, 0x0200U);
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0200U, aif1clk & ~0x0001U);

    g_enabled = 0U;
}

void codec_enter_idle(void)
{
    /* Soft-mute AIF1DAC1 */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0420U, 0x0200U);
    /* Put VMID in low-power standby mode */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0001U, 0x0305U);
}

void codec_exit_idle(void)
{
    /* Put VMID in normal mode */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0001U, 0x0303U);
    /* Un-mute AIF1DAC1 */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0420U, 0x0000U);
}

void codec_set_sample_rate(codec_sample_rate_t sample_rate)
{
    uint32_t aif1clk_cfg, aif1clk_div;

    switch (sample_rate) {
    case CODEC_SAMPLE_RATE_8K:
        /* AIF1CLK sample rate = 8 kHz, ratio = 512 */
        aif1clk_cfg = 0x0005U;
        /* No divider */
        aif1clk_div = 0;
        break;

    case CODEC_SAMPLE_RATE_11_025K:
        /* AIF1CLK sample rate = 11.025 kHz, ratio = 512 */
        aif1clk_cfg = 0x0015U;
        /* No divider */
        aif1clk_div = 0;
        break;

    case CODEC_SAMPLE_RATE_12K:
        /* AIF1CLK sample rate = 12 kHz, ratio = 512 */
        aif1clk_cfg = 0x0025U;
        /* No divider */
        aif1clk_div = 0;
        break;

    case CODEC_SAMPLE_RATE_16K:
        /* AIF1CLK sample rate = 16 kHz, ratio = 256 */
        aif1clk_cfg = 0x0033U;
        /* No divider */
        aif1clk_div = 0;
        break;

    case CODEC_SAMPLE_RATE_22_05K:
        /* AIF1CLK sample rate = 22.05 kHz, ratio = 256 */
        aif1clk_cfg = 0x0043U;
        /* No divider */
        aif1clk_div = 0;
        break;

    case CODEC_SAMPLE_RATE_24K:
        /* AIF1CLK sample rate = 24 kHz, ratio = 256 */
        aif1clk_cfg = 0x0053U;
        /* No divider */
        aif1clk_div = 0;
        break;

    case CODEC_SAMPLE_RATE_32K:
        /* AIF1CLK sample rate = 32 kHz, ratio = 256 */
        aif1clk_cfg = 0x0063U;
        /* No divider */
        aif1clk_div = 0;
        break;

    case CODEC_SAMPLE_RATE_44_1K:
        /* AIF1CLK sample rate = 44.1 kHz, ratio = 256 */
        aif1clk_cfg = 0x0073U;
        /* No divider */
        aif1clk_div = 0;
        break;

    case CODEC_SAMPLE_RATE_48K:
        /* AIF1CLK sample rate = 48 kHz, ratio = 256 */
        aif1clk_cfg = 0x0083U;
        /* No divider */
        aif1clk_div = 0;
        break;

    case CODEC_SAMPLE_RATE_88_2K:
        /* AIF1CLK sample rate = 88.2 kHz, ratio = 128 */
        aif1clk_cfg = 0x0091U;
        /* /2 divider */
        aif1clk_div = 1;
        break;

    case CODEC_SAMPLE_RATE_96K:
        /* AIF1CLK sample rate = 96 kHz, ratio = 128 */
        aif1clk_cfg = 0x000A1U;
        /* /2 divider */
        aif1clk_div = 1;
        break;

    default:
        ASSERT(0);
        __builtin_unreachable();
    }

    /*
     * Update AIF1CLK configuration.
     *
     * No need to read current register value since AIF1CLK is assumed to be
     * disabled.
     */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0200U, aif1clk_div << 1U);
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0210U, aif1clk_cfg);
}

void codec_set_resolution(codec_resolution_t resolution)
{
    uint32_t resol_cfg;

    switch (resolution) {
    case CODEC_RESOLUTION_8:
        i2c4_write_reg16(CODEC_I2C_ADDR, 0x0301U, 0x0008U);
        return;

    case CODEC_RESOLUTION_16:
        resol_cfg = 0b00U;
        break;

    case CODEC_RESOLUTION_20:
        resol_cfg = 0b01U;
        break;

    case CODEC_RESOLUTION_24:
        resol_cfg = 0b10U;
        break;

    case CODEC_RESOLUTION_32:
        resol_cfg = 0b11U;
        break;

    default:
        ASSERT(0);
        __builtin_unreachable();
    }

    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0300U, 0x4010U | (resol_cfg << 5U));
}

void codec_set_volume(uint32_t volume)
{
    ASSERT(volume <= CODEC_MAX_VOLUME);

    if (volume == 0) {
        // TODO: check if soft-mute is working even if overwriting volume

        /* Soft-mute DAC1L/R */
        i2c4_write_reg16_many(CODEC_I2C_ADDR, 0x0610U, 0x0020U, 2U);
        return;
    }

    /* Write new volume for DAC1L */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0610U, volume);
    /* Write new volume for DAC1R and trigger volume update */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0611U, 0x0100U | volume);
}

codec_sample_rate_t codec_sample_rate_from_hz(uint32_t hz)
{
    switch (hz) {
    case 8000U:
        return CODEC_SAMPLE_RATE_8K;

    case 11025U:
        return CODEC_SAMPLE_RATE_11_025K;

    case 12000U:
        return CODEC_SAMPLE_RATE_12K;

    case 16000U:
        return CODEC_SAMPLE_RATE_16K;

    case 22050U:
        return CODEC_SAMPLE_RATE_22_05K;

    case 24000U:
        return CODEC_SAMPLE_RATE_24K;

    case 32000U:
        return CODEC_SAMPLE_RATE_32K;

    case 44100U:
        return CODEC_SAMPLE_RATE_44_1K;

    case 48000U:
        return CODEC_SAMPLE_RATE_48K;

    case 88200U:
        return CODEC_SAMPLE_RATE_88_2K;

    case 96000U:
        return CODEC_SAMPLE_RATE_96K;
    }

    return CODEC_SAMPLE_RATE_INVALID;
}

codec_sample_rate_group_t codec_sample_rate_get_group(codec_sample_rate_t rate)
{
    switch (rate) {
    case CODEC_SAMPLE_RATE_8K:
    case CODEC_SAMPLE_RATE_12K:
    case CODEC_SAMPLE_RATE_16K:
    case CODEC_SAMPLE_RATE_24K:
    case CODEC_SAMPLE_RATE_32K:
    case CODEC_SAMPLE_RATE_48K:
    case CODEC_SAMPLE_RATE_96K:
        return CODEC_SAMPLE_RATE_GROUP_8K;

    case CODEC_SAMPLE_RATE_11_025K:
    case CODEC_SAMPLE_RATE_22_05K:
    case CODEC_SAMPLE_RATE_44_1K:
    case CODEC_SAMPLE_RATE_88_2K:
        return CODEC_SAMPLE_RATE_GROUP_11_025K;

    default:
        ASSERT(0);
        __builtin_unreachable();
    }
}

codec_resolution_t codec_resolution_from_bits(uint32_t bits)
{
    switch (bits) {
    case 8U:
        return CODEC_RESOLUTION_8;

    case 16U:
        return CODEC_RESOLUTION_16;

    case 20U:
        return CODEC_RESOLUTION_20;

    case 24U:
        return CODEC_RESOLUTION_24;

    case 32U:
        return CODEC_RESOLUTION_32;
    }

    return CODEC_RESOLUTION_INVALID;
}

static void wait_end_of_sequence(uint32_t delay_ms)
{
    // TODO: use interrupt ?
    timer_delay(delay_ms);

    /* Busy-wait until control write sequencer is not busy anymore */
    while (i2c4_read_reg16(CODEC_I2C_ADDR, 0x0111U) & 0x0100U) {}

    /* Disable the control write sequencer */
    i2c4_write_reg16(CODEC_I2C_ADDR, 0x0110U, 0x0000U);
}
