#include <string.h>
#include "wav.h"
#include "util.h"

#define WAV_HEADER_SIZE   36U
#define CHUNK_HEADER_SIZE 8U

#define RIFF_ID 0x46464952U
#define WAVE_ID 0x45564157U
#define FMT_ID  0x20746D66U
#define DATA_ID 0x61746164U

#define PCM_FMT 1U

static uint32_t parse_chunk_header(decctx_t* ctx)
{
    /* RIFF file chunks are aligned on 2-byte boundary */
    uint16_t const* chunk_header = decctx_read(ctx, CHUNK_HEADER_SIZE);

    if (chunk_header == NULL) {
        return 1U;
    }

    ctx->wav.chunk_id = (chunk_header[1] << 16) | chunk_header[0];
    ctx->wav.chunk_data_size = (chunk_header[3] << 16) | chunk_header[2];
    ctx->wav.chunk_padding = ctx->wav.chunk_data_size & 0x1U;

    return 0U;
}

uint32_t wav_decode(decctx_t* ctx, void* dest, size_t* dest_size)
{
    size_t remaining_dest_size = *dest_size;
    *dest_size = 0U;

    if (!ctx->wav.riff_header_parsed) {
        uint32_t const* header = decctx_read(ctx, WAV_HEADER_SIZE);

        if (header == NULL) {
            return 0U;
        }

        /* Ensure ID in headers are valid */
        if (header[0] != RIFF_ID || header[2] != WAVE_ID || header[3] != FMT_ID) {
            return 1U;
        }

        /* Only PCM format is supported */
        if ((header[5] & 0x0000FFFFU) != PCM_FMT) {
            return 1U;
        }

        uint32_t channels = (header[5] & 0xFFFF0000U) >> 16U;

        /* Only mono and stereo are supported */
        if (channels == 0U || channels > 2U) {
            return 1U;
        }

        ctx->audio_info.stereo = channels - 1U;
        ctx->audio_info.sample_rate = header[6];
        ctx->audio_info.resolution = (header[8] & 0xFFFF0000U) >> 16U;

        ctx->wav.riff_header_parsed = 1U;
    }

    if (!ctx->wav.decoding_chunk) {
        if (parse_chunk_header(ctx) != 0U) {
            return 0U;
        }

        ctx->wav.decoding_chunk = 1U;
    }

    while (remaining_dest_size != 0U) {
        if (ctx->wav.chunk_id != DATA_ID) {
            size_t full_size = ctx->wav.chunk_data_size + ctx->wav.chunk_padding;

            if (decctx_skip(ctx, full_size) != 0U) {
                return 0U;
            }
        } else {
            uint32_t chunk_data_size = ctx->wav.chunk_data_size;

            if (chunk_data_size != 0U) {
                size_t decoded_size = MIN(chunk_data_size, remaining_dest_size);
                void const* chunk_data = decctx_read_max(ctx, &decoded_size);

                if (decoded_size == 0U) {
                    return 0U;
                }

                memcpy(dest, chunk_data, decoded_size);

                dest += decoded_size;
                *dest_size += decoded_size;
                remaining_dest_size -= decoded_size;

                ctx->wav.chunk_data_size -= decoded_size;
            }

            if (ctx->wav.chunk_padding) {
                if (decctx_skip(ctx, 1U) != 0U) {
                    return 0U;
                }
            }
        }

        if (parse_chunk_header(ctx) != 0U) {
            ctx->wav.decoding_chunk = 0U;
            return 0U;
        }
    }

    ctx->wav.decoding_chunk = 0U;
    return 0U;
}
