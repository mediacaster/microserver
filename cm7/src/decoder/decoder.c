#include "decoder.h"
#include "wav.h"
#include "asrt.h"

void decoder_init(decoder_t* decoder, audiofmt_t fmt)
{
    ASSERT(decoder != NULL);

    decctx_init(&decoder->ctx);

    switch (fmt) {
    case AUDIOFMT_WAV:
        decoder->decoding_func = &wav_decode;
        break;

    // TODO: add others
    default:
        ASSERT(0);
    }
}

void decoder_set_src(decoder_t* decoder, void const* encoded_data, size_t size)
{
    ASSERT(decoder->ctx.buffer_size == 0U);

    decoder->ctx.src = encoded_data;
    decoder->ctx.src_size = size;
}

uint32_t decoder_decode(decoder_t* decoder, void* decoded_data, size_t* max_size)
{
    ASSERT(decoder != NULL);
    ASSERT(decoded_data != NULL);

    if (__builtin_expect(decoder->ctx.src_size == 0U, 0U)) {
        return 0U; /* No data available, return immediately */
    }

    return decoder->decoding_func(&decoder->ctx, decoded_data, max_size);
}
