#include <string.h>
#include "decctx.h"
#include "asrt.h"
#include "util.h"

void decctx_init(decctx_t* ctx)
{
    memset(ctx, 0U, sizeof(decctx_t));
}

void const* decctx_read(decctx_t* ctx, size_t size)
{
    /* Compute remaining size to read */
    size -= ctx->buffer_size;

    size_t read_size = MIN(ctx->src_size, size);
    void const* read_data = ctx->src;

    ctx->src += read_size;
    ctx->src_size -= read_size;

    if (read_size == size && ctx->buffer_size == 0U) {
        return read_data;
    }

    ASSERT(ctx->buffer_size + read_size < DECCTX_READ_BUFFER_SIZE);

    memcpy(&ctx->buffer[ctx->buffer_size], read_data, read_size);

    if (read_size != size) {
        ctx->buffer_size += read_size;
        return NULL;
    }

    ctx->buffer_size = 0U;
    return &ctx->buffer;
}

void const* decctx_read_max(decctx_t* ctx, size_t* max_size)
{
    size_t read_size;

    if (ctx->src_size < *max_size) {
        *max_size = ctx->src_size;
        read_size = ctx->src_size;
    } else {
        read_size = *max_size;
    }

    void const* read_data = ctx->src;

    ctx->src += read_size;
    ctx->src_size -= read_size;

    return read_data;
}

uint32_t decctx_skip(decctx_t* ctx, size_t size)
{
    size -= ctx->buffer_size;

    uint32_t skipped_size = MIN(ctx->src_size, size - ctx->buffer_size);

    ctx->src += skipped_size;
    ctx->src_size -= skipped_size;

    if (skipped_size != size) {
        ctx->buffer_size += skipped_size;
        return 1U;
    }

    ctx->buffer_size = 0U;
    return 0U;
}
