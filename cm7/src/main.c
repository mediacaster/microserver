#include <stdbool.h>
#include <stm32h7xx.h>
#include "rcc.h"
#include "core.h"
#include "asrt.h"
#include "player.h"
#include "sysnotif.h"

/**
 * @brief Configures system clock to 400 MHz.
 */
static void sysclk_config(void);

/**
 * @brief Configures the MPU.
 */
static void mpu_config(void);

// TODO: make MDMA priority lower than CPU1 to avoid bad performance when both
//       try to access DTCM-RAM
// TODO: put critical code in ITCM-RAM
int main(void)
{
    /* Enable L1 cache */
    SCB_EnableICache();
    SCB_EnableDCache();

    /* Wait until CPU2 enters stop mode */
    while (RCC->CR & RCC_CR_D2CKRDY) {}

    /* Configure the system clock */
    sysclk_config();
    /* Configure the MPU */
    mpu_config();

    /* Initialize inter-core notifications */
    sysnotif_init();
    /* Initialize shared player state */
    player_init();

    /* Trigger CPU2 wake up */
    sysnotif_send(SYSNOTIF_SYSTEM_READY_ID);

    /* Wait until CPU2 wakes up */
    while (!(RCC->CR & RCC_CR_D2CKRDY)) {}

    core_init();

    while (true) {
        /* Sleep until an event occurs */
        __WFE();

        core_poll();
    }

    /* Should never be reached */
    return 1;
}

static void sysclk_config(void)
{
    /* Power configuration */
    PWR->CR3 = (PWR->CR3 & ~0x3FU) | PWR_CR3_SMPSEN;
    PWR->D3CR |= PWR_D3CR_VOS_0 | PWR_D3CR_VOS_1;

    while (!(PWR->D3CR & PWR_D3CR_VOSRDY)) {}

    /* Enable HSE oscillator */
    RCC->CR |= RCC_CR_HSEON;

    while (!(RCC->CR & RCC_CR_HSERDY)) {}

    /*
     * FLASH AXI interface clock is 400/2 = 200 MHz so use:
     *     WRHIGHFREQ = 0b10
     *     LATENCY = 2 WS = 0b0010
     */
    FLASH->ACR = (FLASH->ACR & ~0x3FU) | 0x22U;

    while ((FLASH->ACR & 0x3FU) != 0x22U) {}

    /* Configure PLL1 to output 400 MHz clock to CPU1 from 25 MHz HSE */
    RCC->PLLCKSELR = (RCC->PLLCKSELR & ~(RCC_PLLCKSELR_PLLSRC |
                                         RCC_PLLCKSELR_DIVM1)) |
                     (5U << RCC_PLLCKSELR_DIVM1_Pos) | /* M = 5 */
                     RCC_PLLCKSELR_PLLSRC_HSE;         /* SRC = HSE */

    RCC->PLLCFGR = (RCC->PLLCFGR & ~0x0FU) |
                   (0b10U << RCC_PLLCFGR_PLL1RGE_Pos |
                    RCC_PLLCFGR_DIVP1EN);

    RCC->PLL1DIVR = (1U << RCC_PLL1DIVR_P1_Pos) |  /* P = 2 */
                    (159U << RCC_PLL1DIVR_N1_Pos); /* N = 160 */

    RCC->CR |= RCC_CR_PLL1ON;

    while (!(RCC->CR & RCC_CR_PLL1RDY)) {}

    /* Set prescalers */
    RCC->D1CFGR = RCC_D1CFGR_D1CPRE_DIV1 |  /* CPU1 no prescaler */
                  RCC_D1CFGR_HPRE_DIV2 |    /* /2 prescaler for AHB */
                  RCC_D1CFGR_D1PPRE_DIV2;   /* /2 prescaler for APB3 */

    RCC->D2CFGR = RCC_D2CFGR_D2PPRE2_DIV2 | /* /2 prescaler for APB2 */
                  RCC_D2CFGR_D2PPRE1_DIV2;  /* /2 prescaler for APB1 */

    RCC->D3CFGR = RCC_D3CFGR_D3PPRE_DIV2;   /* /2 prescaler for APB4 */

    /* Choose PLL1 as system clock source */
    RCC->CFGR |= RCC_CFGR_SW_PLL1;

    while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL1) {}

    /* Disable HSI and enable CSI, required for I/O compensation cell */
    RCC->CR = (RCC->CR & ~RCC_CR_HSION) | RCC_CR_CSION;

    /* Enable I/O compensation cell */
    rcc_apb4_enable_clock(RCC_APB4ENR_SYSCFGEN);
    SYSCFG->CCCSR = SYSCFG_CCCSR_EN;
    rcc_ahb4_disable_clock(RCC_APB4ENR_SYSCFGEN);

    SystemCoreClockUpdate();
}

static void mpu_config(void)
{
    /* Ensure MPU is present */
    ASSERT(MPU->TYPE != 0U);

    /* MPU region 0 */
    MPU->RNR = 0U;
    /* Region starts 1024 bytes before the end of AXI RAM */
    MPU->RBAR = 0x24080000U - 1024U;
    /* Configure region as shareable and non-cacheable */
    MPU->RASR = MPU_RASR_XN_Msk |
                (0b001U << MPU_RASR_AP_Pos) |
                (0b001U << MPU_RASR_TEX_Pos) |
                MPU_RASR_S_Msk |
                (9U << MPU_RASR_SIZE_Pos) |
                MPU_RASR_ENABLE_Msk;

    MPU->CTRL = MPU_CTRL_PRIVDEFENA_Msk | MPU_CTRL_ENABLE_Msk;
}
