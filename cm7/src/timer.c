#include <stm32h7xx.h>
#include "timer.h"
#include "asrt.h"
#include "rcc.h"

#define DELAY_TIM                 TIM13
#define DELAY_TIM_IRQ_NUM         TIM8_UP_TIM13_IRQn
#define DELAY_TIM_IRQ_HANDLER     TIM8_UP_TIM13_IRQHandler
#define DELAY_TIM_ENABLE_CLOCK()  rcc_apb1l_enable_clock(RCC_APB1LENR_TIM13EN)
#define DELAY_TIM_DISABLE_CLOCK() rcc_apb1l_disable_clock(RCC_APB1LENR_TIM13EN)

static volatile uint32_t g_delay_elapsed = 0U;

// DELAY_TIM is assumed to be clocked with a 200 MHz clock
void timer_delay(uint32_t ms)
{
    ASSERT(ms <= 16384U);
    ASSERT(!(DELAY_TIM->CR1 & TIM_CR1_CEN));

    if (__builtin_expect(ms == 0U, 0)) {
        return;
    }

    DELAY_TIM_ENABLE_CLOCK();

    /* Avoid generating UEV when UG is set */
    DELAY_TIM->CR1 = TIM_CR1_URS;

    /* Set timer prescaler to 50,000 => 4 kHz frequency */
    DELAY_TIM->PSC = 49999U;
    /* Set initial counter value */
    DELAY_TIM->ARR = 4U * ms - 1U;
    /* Reset timer counter and update shadow registers */
    DELAY_TIM->EGR = TIM_EGR_UG;

    /* Clear status register */
    DELAY_TIM->SR = 0U;
    /* Enable update interrupt */
    DELAY_TIM->DIER = TIM_DIER_UIE;
    NVIC_EnableIRQ(DELAY_TIM_IRQ_NUM);

    /* Clear wake-up flag */
    g_delay_elapsed = 0U;

    /* Configure timer in one-pulse mode and start the timer */
    DELAY_TIM->CR1 = TIM_CR1_OPM | TIM_CR1_CEN;

    /* Loop until wake-up */
    do {
        /* Enter CSleep mode */
        __WFE();
    } while (!g_delay_elapsed);

    DELAY_TIM_DISABLE_CLOCK();
}

void DELAY_TIM_IRQ_HANDLER(void)
{
    if (DELAY_TIM->SR & TIM_SR_UIF) {
        /* Clear interrupt flag */
        DELAY_TIM->SR = 0U;
        /* Set wake-up flag */
        g_delay_elapsed = 1U;
    }
}
