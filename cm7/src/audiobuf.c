#include <stddef.h>
#include "audiobuf.h"
#include "asrt.h"
#include "util.h"

void audiobuf_init(audiobuf_t* buf, void* data, uint16_t block_size, uint16_t max_block_count)
{
    ASSERT(buf != NULL);
    ASSERT(data != NULL);
    ASSERT(IS_POW2(block_size));
    ASSERT(IS_POW2(max_block_count));

    buf->added_blocks = 0U;
    buf->removed_blocks = 0U;
    buf->block_size = block_size;
    buf->max_block_count = max_block_count;
    buf->data = data;
    buf->flushing = 0U;

    buf->head_block.size = buf->block_size;
    buf->head_block.data = NULL;

    buf->tail_block.size = 0U;
    buf->tail_block.data = NULL;
}

audiobuf_block_t const* audiobuf_request_put(audiobuf_t* buf)
{
    ASSERT(buf != NULL);
    ASSERT(!buf->flushing);

    if (buf->tail_block.data != NULL) {
        return &buf->tail_block;
    }

    uint32_t used_blocks = buf->added_blocks - buf->removed_blocks;

    if (used_blocks == buf->max_block_count) {
        return NULL;
    }

    uint32_t tail_block_ind = buf->added_blocks & (buf->max_block_count - 1);

    buf->tail_block.data = &buf->data[tail_block_ind * buf->block_size];
    buf->tail_block.size = buf->block_size;

    return &buf->tail_block;
}

void audiobuf_commit_put(audiobuf_t* buf, uint16_t size)
{
    ASSERT(buf != NULL);
    ASSERT(buf->tail_block.data != NULL);
    ASSERT(size <= buf->tail_block.size);
    ASSERT(!buf->flushing);

    uint32_t remaining_block_size = buf->tail_block.size - size;

    if (remaining_block_size == 0U) {
        buf->added_blocks++;
        buf->tail_block.data = NULL;
    } else {
        buf->tail_block.size = remaining_block_size;
    }
}

audiobuf_block_t const* audiobuf_peek(audiobuf_t* buf)
{
    ASSERT(buf != NULL);

    if (buf->head_block.data != NULL) {
        return &buf->head_block;
    }

    if (buf->added_blocks == buf->removed_blocks) {
        if (buf->flushing) {
            uint32_t used_size = buf->block_size - buf->tail_block.size;

            buf->head_block.data = (uint8_t*) buf->tail_block.data - used_size;
            buf->head_block.size = used_size;
            return &buf->head_block;
        }

        return NULL;
    }

    uint32_t head_block_ind = buf->removed_blocks & (buf->max_block_count - 1);
    buf->head_block.data = &buf->data[head_block_ind * buf->block_size];

    return &buf->head_block;
}

void audiobuf_pop(audiobuf_t* buf)
{
    ASSERT(buf != NULL);

    if (buf->removed_blocks == buf->added_blocks) {
        ASSERT(buf->flushing);
        buf->flushing = 0U;
    } else {
        buf->removed_blocks++;
    }

    buf->head_block.data = NULL;
}

void audiobuf_trigger_flush(audiobuf_t* buf)
{
    if (buf->tail_block.data != NULL && buf->tail_block.size != buf->block_size) {
        buf->flushing = 1U;
    }
}

uint32_t audiobuf_is_flushing(audiobuf_t const* buf)
{
    return buf->flushing;
}

void audiobuf_clear(audiobuf_t* buf)
{
    buf->added_blocks = 0U;
    buf->removed_blocks = 0U;
    buf->flushing = 0U;

    buf->head_block.size = buf->block_size;
    buf->head_block.data = NULL;

    buf->tail_block.size = 0U;
    buf->tail_block.data = NULL;
}
