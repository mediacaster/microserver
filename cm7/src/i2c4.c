#include <stm32h7xx.h>
#include "i2c4.h"
#include "rcc.h"

static uint32_t write8(uint32_t val);
static uint32_t write16(uint32_t val);
static uint32_t read8(void);
static uint32_t read16(void);
static uint32_t wait_rxne_or_nack(void);

void i2c4_init(void)
{
    /* Configure I2C4 SDA and SCL pins */

    rcc_ahb4_enable_clock(RCC_AHB4ENR_GPIODEN);

    /* Open-drain */
    GPIOD->OTYPER |= GPIO_OTYPER_OT12 | GPIO_OTYPER_OT13;
    /* Low-speed */
    GPIOD->OSPEEDR &= ~(GPIO_OSPEEDR_OSPEED12 | GPIO_OSPEEDR_OSPEED13);
    /* No pull-up, no pull-down */
    GPIOD->PUPDR &= ~(GPIO_PUPDR_PUPD12 | GPIO_PUPDR_PUPD13);
    /* Alternate functions */
    GPIOD->AFR[1] = (GPIOD->AFR[1] & ~(GPIO_AFRH_AFSEL12 | GPIO_AFRH_AFSEL13))
                    | (4U << GPIO_AFRH_AFSEL12_Pos)
                    | (4U << GPIO_AFRH_AFSEL13_Pos);
    /* Alternate function mode */
    GPIOD->MODER = (GPIOD->MODER & ~(GPIO_MODER_MODE12 | GPIO_MODER_MODE13))
                   | (0b10U << GPIO_MODER_MODE12_Pos)
                   | (0b10U << GPIO_MODER_MODE13_Pos);

    /* Configure I2C4 */

    rcc_apb4_enable_clock(RCC_APB4ENR_I2C4EN);

    /* Ensure I2C is disabled */
    I2C4->CR1 &= ~I2C_CR1_PE;

    /* Disable analog filters and enabled 50 ns digital filters */
    I2C4->CR1 = (I2C4->CR1 & ~I2C_CR1_DNF) |
                (I2C_CR1_ANFOFF | (5U << I2C_CR1_DNF_Pos));

    /*
     * Configure I2C for fast mode at 400 kHz.
     *
     * Assumes 50 pF load capacitance, 200 ns rise time, 17 ns fall time.
     */
    I2C4->TIMINGR = (1U << I2C_TIMINGR_PRESC_Pos)
                    | (14U << I2C_TIMINGR_SCLDEL_Pos)
                    | (0U << I2C_TIMINGR_SDADEL_Pos)
                    | (26U << I2C_TIMINGR_SCLH_Pos)
                    | (79U << I2C_TIMINGR_SCLL_Pos);

    /* Enable I2C */
    I2C4->CR1 |= I2C_CR1_PE;
}

uint32_t i2c4_write_reg16(uint32_t dev_addr, uint32_t reg_addr, uint32_t reg_val)
{
    /* Configure and start transaction */
    I2C4->CR2 = I2C_CR2_AUTOEND
                | (4U << I2C_CR2_NBYTES_Pos)
                | (dev_addr << I2C_CR2_SADD_Pos)
                | I2C_CR2_START;

    if (write16(reg_addr) != 0 || write16(reg_val) != 0) {
        return 1;
    }

    return 0;
}

uint32_t i2c4_write_reg16_many(uint32_t dev_addr, uint32_t reg_addr, uint32_t reg_val,
                               size_t reg_count)
{
    /* Configure and start transaction */
    I2C4->CR2 = I2C_CR2_AUTOEND
                | ((2U + 2U * reg_count) << I2C_CR2_NBYTES_Pos)
                | (dev_addr << I2C_CR2_SADD_Pos)
                | I2C_CR2_START;

    if (write16(reg_addr) != 0) {
        return 1;
    }

    for (size_t i = 0; i < reg_count; ++i) {
        if (write16(reg_val) != 0) {
            return 1;
        }
    }

    return 0;
}

int32_t i2c4_read_reg16(uint32_t dev_addr, uint32_t reg_addr)
{
    /* Configure and start first transfer, AUTOEND is disabled */
    I2C4->CR2 = (2U << I2C_CR2_NBYTES_Pos)
                | (dev_addr << I2C_CR2_SADD_Pos)
                | I2C_CR2_START;

    if (write16(reg_addr) != 0) {
        return -1;
    }

    /* Wait until registry address message is sent */
    while (!(I2C4->ISR & I2C_ISR_TC)) {}

    /* Configure and start second transfer, reading the register value */
    I2C4->CR2 = I2C_CR2_AUTOEND
                | (2U << I2C_CR2_NBYTES_Pos)
                | (dev_addr << I2C_CR2_SADD_Pos)
                | I2C_CR2_RD_WRN
                | I2C_CR2_START;

    /* Ensure no NACK is received */
    if (wait_rxne_or_nack() != 0) {
        return -1;
    }

    return read16();
}

static uint32_t write16(uint32_t val)
{
    /* Send MSB then LSB */
    if (write8(val >> 8) != 0 || write8(val) != 0) {
        return 1;
    }

    return 0;
}

static uint32_t write8(uint32_t val)
{
    uint32_t isr = I2C4->ISR;

    /* Wait until TXIS is set */
    while (!(isr & I2C_ISR_TXIS)) {
        /* Ensure no NACK has been received */
        if (isr & I2C_ISR_NACKF) {
            /* Clear NACK flag */
            I2C4->ICR = I2C_ICR_NACKCF;
            /* Clear TXIS flag */
            I2C4->TXDR = 0;
            /* Flush data transmit register */
            I2C4->ISR |= I2C_ISR_TXE;
            /* Return 1 on error */
            return 1;
        }

        isr = I2C4->ISR;
    }

    I2C4->TXDR = val & 0xFF;
    return 0;
}

static uint32_t read16(void)
{
    return (read8() << 8) | read8();
}

static uint32_t read8(void)
{
    while (!(I2C4->ISR & I2C_ISR_RXNE)) {}

    return I2C4->RXDR;
}

static uint32_t wait_rxne_or_nack(void)
{
    uint32_t isr = I2C4->ISR;

    /* Wait until RXNE is set */
    while (!(isr & I2C_ISR_RXNE)) {
        /* Ensure no NACK has been received */
        if (isr & I2C_ISR_NACKF) {
            /* Clear NACK flag */
            I2C4->ICR = I2C_ICR_NACKCF;
            /* Return 1 */
            return 1;
        }

        isr = I2C4->ISR;
    }

    return 0;
}
