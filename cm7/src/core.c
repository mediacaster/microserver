#include "core.h"
#include "rcc.h"
#include "player.h"
#include "audio.h"
#include "audiobuf.h"
#include "audioinfo.h"
#include "decoder.h"
#include "asrt.h"
#include "util.h"

#define MDMA_IRQ_PRIO        0U
#define DECODED_MDMA_CHANNEL MDMA_Channel0

#define ENCODED_BLOCK_SIZE  PLAYER_AUDIO_BUF_BLOCK_SIZE
#define ENCODED_BLOCK_COUNT PLAYER_AUDIO_BUF_BLOCK_COUNT
#define ENCODED_BUF_SIZE    PLAYER_AUDIO_BUF_SIZE

#define DECODED_BLOCK_SIZE  (32U * 1024U)
#define DECODED_BLOCK_COUNT 2U
#define DECODED_BUF_SIZE    (DECODED_BLOCK_COUNT * DECODED_BLOCK_SIZE)

// Must be a multiple of DECODED_BLOCK_SIZE
#define PLAYBACK_BLOCK_SIZE  (32U * 1024U)
#define PLAYBACK_BLOCK_COUNT 4U
#define PLAYBACK_BUF_SIZE    (PLAYBACK_BLOCK_COUNT * PLAYBACK_BLOCK_SIZE)

__attribute__((section(".dtcm")))
static uint8_t g_encoded_buf[ENCODED_BUF_SIZE];

__attribute__((section(".dtcm")))
static uint8_t g_decoded_buf_data[DECODED_BUF_SIZE];

__attribute__((section(".sram2")))
static uint8_t g_playback_buf_data[PLAYBACK_BUF_SIZE];

static decoder_t g_decoder;
static audiobuf_t g_decoded_buf;
static audiobuf_t g_playback_buf;
static player_playback_state_t g_playback_state;
static uint32_t g_volume;
static uint32_t g_encoded_head;
static uint32_t g_decoded_dma_busy;

static void init_decoded_dma(void);
static void try_publish_decoded(void);
static void play_next(void);
static void stop_playback(void);
static void try_stop_playback(void);

void core_init(void)
{
    ASSERT(g_encoded_buf == PLAYER_AUDIO_BUF_ADDR);

    /* Allocate SRAM2 to CPU1 since playback buffer is in SRAM2 */
    rcc_ahb2_enable_clock(RCC_AHB2ENR_SRAM2EN);

    audiobuf_init(&g_decoded_buf, g_decoded_buf_data, DECODED_BLOCK_SIZE, DECODED_BLOCK_COUNT);

    audiobuf_init(&g_playback_buf, g_playback_buf_data, PLAYBACK_BLOCK_SIZE, PLAYBACK_BLOCK_COUNT);

    audio_init();
    audio_set_volume(100U);

    g_playback_state = PLAYER_PLAYBACK_STOPPED;
    g_volume = 100U;
    g_encoded_head = 0U;
    g_decoded_dma_busy = 0U;

    init_decoded_dma();
}

void core_poll(void)
{
    player_state_t new_state;
    player_get_state(&new_state);

    if (new_state.playback_state != PLAYER_PLAYBACK_PLAYING) {
        if (g_playback_state == new_state.playback_state) {
            return;
        }

        switch (new_state.playback_state) {
        case PLAYER_PLAYBACK_PAUSED:
            audio_pause();
            g_playback_state = PLAYER_PLAYBACK_PAUSED;
            break;

        case PLAYER_PLAYBACK_STOPPING:
            stop_playback();
            break;

        default:
            ASSERT(0);
            break;
        }

        return;
    }

    if (g_volume != new_state.volume) {
        audio_set_volume(new_state.volume);
        g_volume = new_state.volume;
    }

    switch (g_playback_state) {
    case PLAYER_PLAYBACK_PLAYING:
        break;

    case PLAYER_PLAYBACK_PAUSED:
        g_playback_state = PLAYER_PLAYBACK_PLAYING;
        __COMPILER_BARRIER();
        audio_resume();
        break;

    case PLAYER_PLAYBACK_STOPPED:
        g_playback_state = PLAYER_PLAYBACK_PLAYING;
        decoder_init(&g_decoder, new_state.format);
        break;

    default:
        ASSERT(0);
        break;
    }

    uint32_t encoded_block_size = new_state.block_sizes[g_encoded_head];

    /* Check if encoded buffer is empty */
    if (encoded_block_size == 0U) {
        /* If it is and audio end is reached, softly stop playback */
        if (new_state.eof) {
            try_stop_playback();
        }

        return;
    }

    if (!decoder_has_data(&g_decoder)) {
        void* encoded_block = &g_encoded_buf[g_encoded_head];
        decoder_set_src(&g_decoder, encoded_block, encoded_block_size);
    }

    /*
     * Loop until the current block is fully decoded or the decoding process
     * must be stalled.
     */
    do {
        audiobuf_block_t const* decoded_block = audiobuf_request_put(&g_decoded_buf);

        /* Ensure decoded buffer is not full */
        if (decoded_block == NULL) {
            break; /* If it is, stall the decoding process */
        }

        size_t decoded_size = decoded_block->size;

        /* Decode next audio samples */
        uint32_t err = decoder_decode(&g_decoder, decoded_block->data, &decoded_size);

        /* In case an error has occurred */
        if (err != 0U) {
            // TODO: handle error : stop playback and notify CPU2
        }

        /* In case some audio was decoded */
        if (decoded_size != 0U) {
            audiobuf_commit_put(&g_decoded_buf, decoded_size);
            try_publish_decoded();
        }
    } while (decoder_has_data(&g_decoder));

    /* In case the current encoded block has been fully decoded */
    if (!decoder_has_data(&g_decoder)) {
        player_notify_block_processed(g_encoded_head);
        g_encoded_head = (g_encoded_head + 1) % ENCODED_BLOCK_COUNT;
    }
}

void MDMA_IRQHandler(void)
{
    if (!(DECODED_MDMA_CHANNEL->CISR & MDMA_CIFCR_CCTCIF)) {
        return;
    }

    /* Clear all interrupt flags for the channel */
    DECODED_MDMA_CHANNEL->CIFCR = 0x0000001FU;

    /* Retrieve the block that has been copied */
    audiobuf_block_t const* decoded_block = audiobuf_peek(&g_decoded_buf);

    audiobuf_commit_put(&g_playback_buf, decoded_block->size);
    audiobuf_pop(&g_decoded_buf);

    if (!audio_is_busy()) {
        if (audio_is_stopped()) {
            audioinfo_t const* audio_info = decoder_get_audio_info(&g_decoder);

            // TODO: avoid calling that in ISR if possible
            uint32_t err = audio_config(audio_info->sample_rate, audio_info->resolution,
                                        !audio_info->stereo);

            if (err != 0U) {
                // TODO: handle error (stop playback + notify CPU2)
            }
        }

        play_next();
    }

    g_decoded_dma_busy = 0U;
    __COMPILER_BARRIER();

    /* Try to start the copy of next decoded block to the playback buffer */
    try_publish_decoded();
}

void audio_transfer_complete(void)
{
    audiobuf_pop(&g_playback_buf);

    /*
     * Try to copy next decoded block to playback buffer since the transfers
     * from decoded buffer to playback buffer could have been stopped in case
     * the playback buffer was full.
     */
    try_publish_decoded();

    /* If the playback is not paused, play the next available audio data */
    if (g_playback_state == PLAYER_PLAYBACK_PLAYING) {
        play_next();
    }
}

static void init_decoded_dma(void)
{
    rcc_ahb3_enable_clock(RCC_AHB3ENR_MDMAEN);

    ASSERT(!(DECODED_MDMA_CHANNEL->CCR & MDMA_CCR_EN));

    DECODED_MDMA_CHANNEL->CCR = 0b10U << MDMA_CCR_PL_Pos; /* High priority */

    /* Configure channel transfer */
    DECODED_MDMA_CHANNEL->CTCR = MDMA_CTCR_BWM |                   /* Destination write is bufferable */
                                 MDMA_CTCR_SWRM |                  /* Transfer triggered by software */
                                 (0b01U << MDMA_CTCR_TRGM_Pos) |   /* Block transfer */
                                 (127U << MDMA_CTCR_TLEN_Pos) |    /* Transfer length = 128 bytes */
                                 (0b10U << MDMA_CTCR_DINCOS_Pos) | /* Dest. increment = 32 bits */
                                 (0b10U << MDMA_CTCR_SINCOS_Pos) | /* Src. increment = 32 bits */
                                 (0b10U << MDMA_CTCR_DSIZE_Pos) |  /* Dest. size = 32 bits */
                                 (0b10U << MDMA_CTCR_SSIZE_Pos) |  /* Src. size = 32 bits */
                                 (0b10U << MDMA_CTCR_DINC_Pos) |   /* Enable dest. increment */
                                 (0b10U << MDMA_CTCR_SINC_Pos);    /* Enable src. increment */

    DECODED_MDMA_CHANNEL->CTBR = MDMA_CTBR_DBUS |                  /* Dest. is on AHB bus */
                                 MDMA_CTBR_SBUS;                   /* Src. is on TCM bus */

    /* Clear all interrupt flags for the channel */
    DECODED_MDMA_CHANNEL->CIFCR = 0x0000001FU;

    NVIC_SetPriority(MDMA_IRQn, MDMA_IRQ_PRIO);
    NVIC_EnableIRQ(MDMA_IRQn);
}

static void try_publish_decoded(void)
{
    if (g_decoded_dma_busy) {
        return;
    }

    audiobuf_block_t const* decoded_block = audiobuf_peek(&g_decoded_buf);

    /* Ensure decoded buffer is not empty */
    if (decoded_block == NULL) {
        return;
    }

    audiobuf_block_t const* playback_block = audiobuf_request_put(&g_playback_buf);

    /* Ensure there is free space in playback buffer */
    if (playback_block == NULL) {
        return;
    }

    /*
     * All decoded blocks have the same size (which is a multiple of 4 bytes)
     * except the last which could be of any size. To ensure the block content
     * can be transfered by DMA word by word, the size must be a multiple of
     * 4 bytes.
     */
    uint32_t block_size = ALIGN4(decoded_block->size);

    ASSERT(playback_block->size >= block_size);
    ASSERT(!(DECODED_MDMA_CHANNEL->CCR & MDMA_CCR_EN));

    /* Set number of bytes to transfer */
    DECODED_MDMA_CHANNEL->CBNDTR = block_size << MDMA_CBNDTR_BNDT_Pos;
    /* Set transfer source address */
    DECODED_MDMA_CHANNEL->CSAR = (uint32_t) decoded_block->data;
    /* Set transfer destination address */
    DECODED_MDMA_CHANNEL->CDAR = (uint32_t) playback_block->data;

    /* Set busy flag */
    g_decoded_dma_busy = 1U;
    __COMPILER_BARRIER();

    /* Clear all interrupt flags for the channel */
    DECODED_MDMA_CHANNEL->CIFCR = 0x0000001FU;
    /* Enable channel transfer complete interrupt and start the transfer */
    DECODED_MDMA_CHANNEL->CCR = MDMA_CCR_CTCIE | MDMA_CCR_EN | MDMA_CCR_SWRQ;
}

static void play_next(void)
{
    ASSERT(!audio_is_busy());

    audiobuf_block_t const* playback_block = audiobuf_peek(&g_playback_buf);

    if (playback_block == NULL) {
        return;
    }

    audio_play(playback_block->data, playback_block->size);
}

static void stop_playback(void)
{
    player_notify_stopped();

    audio_stop();

    /* If decoded data is currently being transfered */
    if (DECODED_MDMA_CHANNEL->CCR & MDMA_CCR_EN) {
        /* Disable CTC interrupt and stop current MDMA transfer */
        DECODED_MDMA_CHANNEL->CCR &= ~(MDMA_CCR_CTCIE | MDMA_CCR_EN);
        /* Wait until channel transfer complete flag is set */
        while (!(DECODED_MDMA_CHANNEL->CISR & MDMA_CIFCR_CCTCIF)) {}
    }

    __COMPILER_BARRIER();
    // TODO: reset g_encoded_head ?
    g_decoded_dma_busy = 0U;
    g_playback_state = PLAYER_PLAYBACK_STOPPED;
    audiobuf_clear(&g_decoded_buf);
    audiobuf_clear(&g_playback_buf);
}

// Softly stop the playback
static void try_stop_playback(void)
{
    /* Flush decoded buffer */
    audiobuf_trigger_flush(&g_decoded_buf);
    /* Ensure decoded data is transfered to playback buffer */
    try_publish_decoded();

    /* Do not continue until decoded buffer is flushed */
    if (audiobuf_is_flushing(&g_decoded_buf)) {
        return;
    }

    /* Flush playback buffer */
    audiobuf_trigger_flush(&g_playback_buf);

    /* Ensure last playback data is played */
    if (!audio_is_busy()) {
        play_next();
    }

    /* Do not continue until playback buffer is flushed */
    if (audiobuf_is_flushing(&g_playback_buf)) {
        return;
    }

    stop_playback();
}
