#include <stm32h7xx.h>
#include "audio.h"
#include "codec.h"
#include "rcc.h"
#include "asrt.h"

#define DMA_ENABLE_CLOCK()     rcc_ahb1_enable_clock(RCC_AHB1ENR_DMA1EN)
#define DMA_STREAM_ISR         DMA1->LISR
#define DMA_STREAM_TCIF_MASK   DMA_LISR_TCIF0
#define DMA_STREAM_IFCR        (DMA1->LIFCR)
#define DMA_STREAM_IFCR_MASK   0x0000003DU
#define DMA_STREAM_IRQ_HANDLER DMA1_Stream0_IRQHandler
#define DMA_STREAM_IRQ_NUM     DMA1_Stream0_IRQn
#define DMA_STREAM_IRQ_PRIO    0U
#define DMA_STREAM             DMA1_Stream0
#define DMAMUX_CHANNEL         DMAMUX1_Channel0
#define DMAMUX_CHANNEL_STATUS  DMAMUX1_ChannelStatus
#define DMAMUX_CHANNEL_IND     0U

/**
 * @brief Current audio sample rate.
 *
 * This must be initialized to a value different from the actual default sample
 * rate (48 kHz).
 */
static codec_sample_rate_t g_cur_sample_rate = CODEC_SAMPLE_RATE_INVALID;

/**
 * @brief Flag indicating whether data is currently being transfered to the codec.
 *
 * This flag is set to 1 iff data is currently being transfered to the audio
 * codec, meaning the audio_play() function cannot be called. This flag is not
 * cleared when playback is paused.
 */
static volatile uint32_t g_busy = 0U;

/**
 * @brief Flag indicating whether playback is stopped.
 */
static volatile uint32_t g_stopped = 1U;

static void disable_sai(void);
static void init_sai_pins(void);

/* SAI and SAI MCLK must be disabled */
static void config_clock(codec_sample_rate_t sample_rate);

/**
 * @brief Configures PLL2 according to a given audio sample rate.
 * @param sample_rate The sample rate.
 *
 * For 8 kHz, 12 kHz, 16 kHz, 24 kHz, 32 kHz, 48 kHz, and 96 kHz sample rates
 * the PLL2 is configured for a 24.576 Mhz output frequency.
 *
 * For 11.025 kHz, 22.05 kHz, 44.1 kHz and 88.2 kHz sample rates the PLL2 is
 * configured for a 22.5792 MHz output frequency.
 *
 * This functions assumes the PLL2 source is a 25 MHz clock (HSE).
 */
static void config_pll(codec_sample_rate_t sample_rate);

static void init_dma(void);
static void enter_idle(void);
static void exit_idle(void);
static void enable_mclk(void);
static void disable_mclk(void);

void audio_init(void)
{
    /* Ensure HSE is ready and selected as PLL source */
    ASSERT(RCC->CR & RCC_CR_HSERDY);
    ASSERT(((RCC->PLLCKSELR & RCC_PLLCKSELR_PLLSRC) >> RCC_PLLCKSELR_PLLSRC_Pos)
           == RCC_PLLCKSELR_PLLSRC_HSE);

    /* Configure SAI1A pins */
    init_sai_pins();

    /* Enable SAI1 clock */
    rcc_apb2_enable_clock(RCC_APB2ENR_SAI1EN);

    /* Ensure SAI1A is disabled */
    disable_sai();

    /*
     * Set SAI configuration registers.
     *
     * Configured as master transmitter with free protocol, DMA, 24 bits default
     * data size, and data are transferred with MSB first. No companding is used
     * and FIFO threshold is set to 1/2.
     */
    SAI1_Block_A->CR1 = (0b110U << SAI_xCR1_DS_Pos) | SAI_xCR1_DMAEN;
    SAI1_Block_A->CR2 = 0b010U << SAI_xCR2_FTH_Pos;

    /* Select PLL2P output as SAI1 kernel clock */
    RCC->D2CCIP1R = (RCC->D2CCIP1R & ~RCC_D2CCIP1R_SAI1SEL) |
                    (0b001U << RCC_D2CCIP1R_SAI1SEL_Pos);

    /* Configure SAI frames for I2S, default is 24-bit stereo audio */
    SAI1_Block_A->FRCR = SAI_xFRCR_FSOFF |
                         SAI_xFRCR_FSDEF |
                         (23U << SAI_xFRCR_FSALL_Pos) |
                         (47U << SAI_xFRCR_FRL_Pos);

    /* Configure SAI slots for non-TDM stereo */
    SAI1_Block_A->SLOTR = (0x0003U << SAI_xSLOTR_SLOTEN_Pos) |
                          (1U << SAI_xSLOTR_NBSLOT_Pos);

    /* Initialize DMA for SAI */
    init_dma();

    /* Initialize audio codec */
    codec_init();
    /* Configure SAI for 12.288 MHz MCLK output */
    config_clock(CODEC_SAMPLE_RATE_48K);
    /* Enable SAI MCLK clock */
    enable_mclk();
    /* Run codec cold startup sequence */
    codec_enable();

    /*
     * SAI is disabled and the codec is initially in idle mode, no need to call
     * enter_idle().
     */
}

void audio_enter_standby(void)
{
    /* Ensure SAI is disabled */
    ASSERT(!(SAI1_Block_A->CR1 & SAI_xCR1_SAIEN));

    /* Exit idle mode */
    exit_idle();
    /* Power the codec off */
    codec_power_off();
    /* Disable MCLK */
    disable_mclk();
    /* Disable SAI clock */
    rcc_apb2_disable_clock(RCC_APB2ENR_SAI1EN);
}

void audio_exit_standby(void)
{
    /* Re-enable SAI clock */
    rcc_apb2_enable_clock(RCC_APB2ENR_SAI1EN);
    /* Re-enable MCLK for codec re-initilization */
    enable_mclk();
    /* Re-initialize codec */
    codec_power_on();
}

uint32_t audio_config(uint32_t sample_rate, uint32_t resolution, uint32_t mono)
{
    /* Ensure stopped */
    ASSERT(g_stopped);
    /* Ensure SAI is disabled */
    ASSERT(!(SAI1_Block_A->CR1 & SAI_xCR1_SAIEN));

    sample_rate = codec_sample_rate_from_hz(sample_rate);

    if (sample_rate == CODEC_SAMPLE_RATE_INVALID) {
        return 1U;
    }

    resolution = codec_resolution_from_bits(resolution);

    if (resolution == CODEC_RESOLUTION_INVALID) {
        return 1U;
    }

    if (sample_rate != g_cur_sample_rate) {
        /* Ensure codec is disabled */
        codec_disable();
        disable_mclk();

        /* Configure the SAI clock according to the sample rate */
        config_clock(sample_rate);
        /* Configure codec sample rate */
        codec_set_sample_rate(sample_rate);

        g_cur_sample_rate = sample_rate;
    }

    uint32_t ds, psize;

    switch (resolution) {
    case CODEC_RESOLUTION_8:
        ds = 0b010U;
        psize = 0b00U; /* byte */
        break;

    case CODEC_RESOLUTION_16:
        ds = 0b100U;
        psize = 0b01U; /* half-word */
        break;

    case CODEC_RESOLUTION_20:
        ds = 0b101U;
        psize = 0b10U; /* word */
        break;

    case CODEC_RESOLUTION_24:
        ds = 0b110U;
        psize = 0b10U; /* word */
        break;

    case CODEC_RESOLUTION_32:
        ds = 0b111U;
        psize = 0b10U; /* word */
        break;

    default:
        ASSERT(0);
        __builtin_unreachable();
    }

    /* Configure SAI DMA PSIZE */
    DMA_STREAM->CR = (DMA_STREAM->CR & ~DMA_SxCR_PSIZE) |
                     (psize << DMA_SxCR_PSIZE_Pos);

    /* Configure SAI data size and mono mode */
    SAI1_Block_A->CR1 = (SAI1_Block_A->CR1 & ~(SAI_xCR1_MONO | SAI_xCR1_DS)) |
                        (mono << SAI_xCR1_MONO_Pos) |
                        (ds << SAI_xCR1_DS_Pos);

    /* Configure SAI frame */
    SAI1_Block_A->FRCR = (SAI1_Block_A->FRCR & ~(SAI_xFRCR_FSALL | SAI_xFRCR_FRL)) |
                         ((resolution - 1U) << SAI_xFRCR_FSALL_Pos) |
                         ((2U * resolution - 1U) << SAI_xFRCR_FRL_Pos);

    /* Configure codec audio resolution */
    codec_set_resolution(resolution);

    /* Return 0 on success */
    return 0U;
}

void audio_play(void const* data, size_t size)
{
    /* Ensure no transfer to the audio codec is ongoing */
    ASSERT(!g_busy);
    /* Ensure DMA is disabled */
    ASSERT(!(DMA_STREAM->CR & DMA_SxCR_EN));

    /* Clear stopped flag */
    g_stopped = 0U;
    /* Set busy flag */
    g_busy = 1U;

    uint32_t msize;
    uint32_t alignment = (uint32_t) data | size;

    /* Choose higher MSIZE possible to get best performance */
    if ((alignment & 0x3U) == 0) {
        /* Start and end addresses are 4-byte aligned */
        msize = 0b10U; /* Read data word by word */
    } else if ((alignment & 0x1U) == 0) {
        /* Start and end addresses are 2-byte aligned */
        msize = 0b01U; /* Read data half-word by half-word */
    } else {
        /* Start and end addresses are byte aligned */
        msize = 0b00U; /* Read data byte by byte */
    }

    uint32_t dma_cr = DMA_STREAM->CR;

    /* Configure MSIZE of DMA stream */
    DMA_STREAM->CR = (dma_cr & ~DMA_SxCR_MSIZE) |
                     (msize << DMA_SxCR_MSIZE_Pos);

    /* Set memory base address */
    DMA_STREAM->M0AR = (uint32_t) data;

    uint32_t psize = (dma_cr & DMA_SxCR_PSIZE) >> DMA_SxCR_PSIZE_Pos;

    /* Ensure data size can be handled by DMA */
    ASSERT((size >> psize) <= 0xFFFFU);
    /* Ensure alignment of start and end addresses is valid according to PSIZE */
    ASSERT((alignment & ((1U << psize) - 1U)) == 0U);

    /*
     * Set number of items.
     *
     * If n is the number of bytes corresponding to DMA PSIZE value, then
     * n = 2^(PSIZE).
     */
    DMA_STREAM->NDTR = size >> psize;

    /* Clear DMA stream interrupt flags */
    DMA_STREAM_IFCR = DMA_STREAM_IFCR_MASK;

    /* Ensure not in idle mode */
    if (!(SAI1_Block_A->CR1 & SAI_xCR1_SAIEN)) {
        exit_idle();
    }

    /* Enable DMA stream and transfer complete interrupt */
    DMA_STREAM->CR |= DMA_SxCR_EN | DMA_SxCR_TCIE;
}

void audio_pause(void)
{
    /* Assumes playback is running */
    ASSERT(SAI1_Block_A->CR1 & SAI_xCR1_SAIEN);

    uint32_t dma_cr = DMA_STREAM->CR;

    /* Disable DMA transfer and prevent TC interrupt from being triggered */
    DMA_STREAM->CR = dma_cr & ~(DMA_SxCR_EN | DMA_SxCR_TCIE);
    /* Wait until transfer is effectively stopped */
    while (DMA_STREAM->CR & DMA_SxCR_EN) {}

    /* Get DMA PSIZE value */
    uint32_t psize = (dma_cr & DMA_SxCR_PSIZE) >> DMA_SxCR_PSIZE_Pos;
    /* Get the number of remaining items */
    uint32_t remaining_items = DMA_STREAM->NDTR;

    /* Check if DMA transfer has been completed */
    if (remaining_items == 0) {
        /* If so, ensure busy flag is cleared */
        g_busy = 0U;
    } else {
        /* Update DMA memory base address with the next read memory location */
        DMA_STREAM->M0AR += remaining_items << psize;
    }

    /* Enter idle mode to reduce power consumption */
    enter_idle();
}

void audio_resume(void)
{
    /* Ensure DMA is disabled */
    ASSERT(!(DMA_STREAM->CR & DMA_SxCR_EN));

    /*
     * Check if a DMA transfer was ongoing when playback was paused.
     *
     * If a transfer was ongoing the transfer has been stopped with the transfer
     * complete interrupt masked so the transfer complete flag is set.
     *
     * If no transfer was ongoing then the transfer complete interrupt has been
     * called before pausing the playback so the transfer complete flag is
     * clear.
     */
    if (!(DMA_STREAM_ISR & DMA_STREAM_TCIF_MASK)) {
        return;
    }

    /* Clear DMA transfer complete interrupt flag */
    DMA_STREAM_IFCR = DMA_STREAM_IFCR_MASK;

    /*
     * Check if the DMA transfer has been completed during the pausing
     * process.
     */
    if (g_busy) {
        /* If not, there is still data to transfer, re-enable the DMA */
        DMA_STREAM->CR |= DMA_SxCR_EN | DMA_SxCR_TCIE;
        /* Exit idle mode */
        exit_idle();
    } else {
        /*
         * Otherwise, the last piece of data of the transfer have been
         * transferred when DMA was stopped in audio_pause(). The transfer is
         * therefore complete but the transfer complete interrupt has not been
         * called so the callback need to be called here.
         */
        audio_transfer_complete();
    }
}

void audio_stop(void)
{
    /* Disable DMA transfer and prevent TC interrupt from being triggered */
    DMA_STREAM->CR &= ~(DMA_SxCR_EN | DMA_SxCR_TCIE);
    /* Wait until transfer is effectively stopped */
    while (DMA_STREAM->CR & DMA_SxCR_EN) {}

    /* Enter idle mode to reduce power consumption */
    enter_idle();
    /* Flush SAI1A FIFO */
    SAI1_Block_A->CR2 |= SAI_xCR2_FFLUSH;

    /* Ensure busy flag is cleared */
    g_busy = 0U;
    /* Set stopped flag */
    g_stopped = 1U;
}

uint32_t audio_is_busy(void)
{
    return g_busy;
}

uint32_t audio_is_stopped(void)
{
    return g_stopped;
}

__WEAK void audio_transfer_complete(void)
{
    /* Empty */
}

void DMA_STREAM_IRQ_HANDLER(void)
{
    /* The only enabled DMA interrupt is the transfer complete interrupt */

    /* Clear interrupt flag */
    DMA_STREAM_IFCR = DMA_STREAM_IFCR_MASK;
    /* Clear busy flag */
    g_busy = 0U;
    /* Call callback */
    audio_transfer_complete();
}

static void disable_sai(void)
{
    /* Disable SAI1A */
    SAI1_Block_A->CR1 &= ~SAI_xCR1_SAIEN;
    /* Wait until SAI1A is disabled */
    while (SAI1_Block_A->CR1 & SAI_xCR1_SAIEN) {}
}

static void init_sai_pins(void)
{
    /* Enable GPIO clocks for SAI1A pins */
    rcc_ahb4_enable_clock(RCC_AHB4ENR_GPIOEEN | RCC_AHB4ENR_GPIOGEN);

    /* Configure SAI1A pins in push-pull */
    GPIOE->OTYPER &= ~(GPIO_OTYPER_OT4 | GPIO_OTYPER_OT5 | GPIO_OTYPER_OT6);
    GPIOG->OTYPER &= ~GPIO_OTYPER_OT7;

    /*
     * PE4 is FS pin, max frequency is 96 kHz (maximum audio sample rate).
     *
     * PE5 and PE6 are respectively SCK and SD pins, max frequency is
     * 96*2*32 = 6.144 Mhz, which corresponds to a stereo audio signal with 32
     * bits resolution and 96 kHz sample rate.
     */
    GPIOE->OSPEEDR &= ~(GPIO_OSPEEDR_OSPEED4 | GPIO_OSPEEDR_OSPEED5 |
                        GPIO_OSPEEDR_OSPEED6);

    /* PG7 is MCLK pin, max frequency is 24.576 MHz */
    GPIOG->OSPEEDR = (GPIOG->OSPEEDR & ~(GPIO_OSPEEDR_OSPEED7)) |
                     (0b01U << GPIO_OSPEEDR_OSPEED7_Pos);

    /* Neither pull-up nor pull-down */
    GPIOE->PUPDR &= ~(GPIO_PUPDR_PUPD4 | GPIO_PUPDR_PUPD5 | GPIO_PUPDR_PUPD6);
    GPIOG->PUPDR &= ~GPIO_PUPDR_PUPD7;

    /* Choose alternate functions */
    GPIOE->AFR[0] = (GPIOE->AFR[0] & ~(GPIO_AFRL_AFSEL4 |
                                       GPIO_AFRL_AFSEL5 |
                                       GPIO_AFRL_AFSEL6)) |
                    (6U << GPIO_AFRL_AFSEL4_Pos) |
                    (6U << GPIO_AFRL_AFSEL5_Pos) |
                    (6U << GPIO_AFRL_AFSEL6_Pos);

    GPIOG->AFR[0] = (GPIOG->AFR[0] & ~GPIO_AFRL_AFSEL7) |
                    (6U << GPIO_AFRL_AFSEL7_Pos);

    /* Configure SAI1A pins in alternate function mode */
    GPIOE->MODER = (GPIOE->MODER & ~(GPIO_MODER_MODE4 |
                                     GPIO_MODER_MODE5 |
                                     GPIO_MODER_MODE6)) |
                   (0b10U << GPIO_MODER_MODE4_Pos) |
                   (0b10U << GPIO_MODER_MODE5_Pos) |
                   (0b10U << GPIO_MODER_MODE6_Pos);

    GPIOG->MODER = (GPIOG->MODER & ~GPIO_MODER_MODE7) |
                   (0b10U << GPIO_MODER_MODE7_Pos);
}

static void config_clock(codec_sample_rate_t sample_rate)
{
    /* Configure the PLL */
    config_pll(sample_rate);

    uint32_t osr, mckdiv;

    switch (sample_rate) {
    case CODEC_SAMPLE_RATE_8K:
    case CODEC_SAMPLE_RATE_12K:
        /* MCLK = 512 * FS */
        osr = 1U;
        /* MCLK = PLL2P / ((96000 / 2) / sample_rate) */
        mckdiv = (CODEC_SAMPLE_RATE_96K / 2U) / sample_rate;
        break;

    case CODEC_SAMPLE_RATE_11_025K:
        /* MCLK = 512 * FS */
        osr = 1U;
        /* MCLK = PLL2P / 4 */
        mckdiv = 4U;
        break;

    case CODEC_SAMPLE_RATE_16K:
    case CODEC_SAMPLE_RATE_24K:
    case CODEC_SAMPLE_RATE_32K:
    case CODEC_SAMPLE_RATE_48K:
    case CODEC_SAMPLE_RATE_96K:
        /* MCLK = 256 * FS */
        osr = 0U;
        /* MCLK = PLL2P / (96000 / sample_rate) */
        mckdiv = CODEC_SAMPLE_RATE_96K / sample_rate;
        break;

    case CODEC_SAMPLE_RATE_22_05K:
    case CODEC_SAMPLE_RATE_44_1K:
    case CODEC_SAMPLE_RATE_88_2K:
        /* MCLK = 256 * FS */
        osr = 0U;
        /* MCLK = PLL2P / (88200 / sample_rate) */
        mckdiv = CODEC_SAMPLE_RATE_88_2K / sample_rate;
        break;

    default:
        ASSERT(0);
        __builtin_unreachable();
    }

    /* Configure SAI clock */
    SAI1_Block_A->CR1 = (SAI1_Block_A->CR1 & ~0x07F80000U) |
                        (mckdiv << SAI_xCR1_MCKDIV_Pos) |
                        (osr << SAI_xCR1_OSR_Pos);
}

static void config_pll(codec_sample_rate_t sample_rate)
{
    uint32_t new_sr_group = codec_sample_rate_get_group(sample_rate);
    uint32_t old_sr_group = codec_sample_rate_get_group(g_cur_sample_rate);

    /*
     * There are two PLL configurations, one for sample rates that are multiple
     * of 8 kHz and one for those that are multiple of 11.025 kHz.
     *
     * To avoid modifying PLL configuration if not needed it is checked here
     * if the two samples rates are both multiple of 8, or both non-multiple
     * of 8.
     */
    if (new_sr_group == old_sr_group) {
        return;
    }

    /* Ensure PLL2 is disabled */
    RCC->CR &= ~RCC_CR_PLL2ON;
    /* Wait until PLL2 is disabled */
    while (RCC->CR & RCC_CR_PLL2RDY) {}

    uint32_t fracn, p, n;

    switch (new_sr_group) {
    case CODEC_SAMPLE_RATE_GROUP_8K:
        /* Configure for a 24.576 MHz output */
        fracn = 524U;
        p = 13U;
        n = 343U;
        break;

    case CODEC_SAMPLE_RATE_GROUP_11_025K:
        /* Configure for a 22.5792 MHz output */
        fracn = 891U;
        p = 13U;
        n = 315U;
        break;

    default:
        ASSERT(0);
        __builtin_unreachable();
    }

    RCC->PLLCKSELR = (RCC->PLLCKSELR & ~RCC_PLLCKSELR_DIVM2) |
                     (25U << RCC_PLLCKSELR_DIVM2_Pos);  /* M = 25 */

    RCC->PLL2FRACR = fracn << RCC_PLL2FRACR_FRACN2_Pos; /* Set FRACN */

    RCC->PLLCFGR = (RCC->PLLCFGR & ~0xF0U) |
                   (RCC_PLLCFGR_PLL2FRACEN |     /* Use fractional */
                    RCC_PLLCFGR_PLL2VCOSEL |     /* Use VCOL */
                    RCC_PLLCFGR_DIVP2EN);        /* Enable P output */

    RCC->PLL2DIVR = (p << RCC_PLL1DIVR_P1_Pos) | /* Set P */
                    (n << RCC_PLL1DIVR_N1_Pos);  /* Set N */
}

static void init_dma(void)
{
    DMA_ENABLE_CLOCK();

    /* Ensure stream is disabled */
    DMA_STREAM->CR &= ~DMA_SxCR_EN;
    /* Wait until the stream is ready to be configured */
    while (DMA_STREAM->CR & DMA_SxCR_EN) {}

    /* Clear all interrupt flags */
    DMA_STREAM_IFCR = DMA_STREAM_IFCR_MASK;

    /* Set peripheral port register address */
    DMA_STREAM->PAR = (uint32_t) &SAI1_Block_A->DR;

    /* Configure DMAMUX channel */
    DMAMUX_CHANNEL->CCR = (87U << DMAMUX_CxCR_DMAREQ_ID_Pos);
    /* Clear DMAMUX synchronization overrun event flag */
    DMAMUX_CHANNEL_STATUS->CFR = 1U << DMAMUX_CHANNEL_IND;

    /* Configure DMA FIFO */
    DMA_STREAM->FCR = DMA_SxFCR_DMDIS |             /* Enable FIFO */
                      (0b11U << DMA_SxFCR_FTH_Pos); /* FIFO threshold = full */

    /* Configure DMA stream, default audio data size is 24 bits */
    DMA_STREAM->CR = (0b10U << DMA_SxCR_PL_Pos) |    /* High priority */
                     (0b10U << DMA_SxCR_PSIZE_Pos) | /* PSIZE = 32 bits */
                     (0b01U << DMA_SxCR_DIR_Pos) |   /* Memory-to-peripheral */
                     DMA_SxCR_MINC;                  /* Memory increment mode */

    NVIC_SetPriority(DMA_STREAM_IRQ_NUM, DMA_STREAM_IRQ_PRIO);
    NVIC_EnableIRQ(DMA_STREAM_IRQ_NUM);
}

static void enter_idle(void)
{
    /* Disable SAI to reduce power consumption */
    disable_sai();
    /* Put codec in idle mode */
    codec_enter_idle();
}

static void exit_idle(void)
{
    /*
     * Ensure MCLK clock and codec are enabled.
     * They could be disabled if the sample rate was changed.
     */
    enable_mclk();
    codec_enable();

    /* Make codec exit idle mode */
    codec_exit_idle();
    /* Re-enable SAI */
    SAI1_Block_A->CR1 |= SAI_xCR1_SAIEN;
}

static void enable_mclk(void)
{
    /* Enable PLL2 */
    RCC->CR |= RCC_CR_PLL2ON;
    /* Wait until PLL2 is ready */
    while (!(RCC->CR & RCC_CR_PLL2RDY)) {}

    /* Enable MCLK clock */
    SAI1_Block_A->CR1 |= SAI_xCR1_MCKEN;
}

static void disable_mclk(void)
{
    /* Disable SAI MCLK */
    SAI1_Block_A->CR1 &= ~SAI_xCR1_MCKEN;
    /* Disable PLL2 */
    RCC->CR &= ~RCC_CR_PLL2ON;
}
