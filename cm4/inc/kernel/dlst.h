/**
 * @file
 * @brief Circular doubly linked list, optimized for use in scheduler.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_DLST_H
#define KERNEL_DLST_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "kernel.h"

/**
 * @struct
 * @brief An element of a circular doubly linked list.
 */
typedef struct dlst_item {
    struct dlst_item* prev; /**< Previous element in the list */
    struct dlst_item* next; /**< Next element in the list */
} dlst_item_t;

/**
 * @struct
 * @brief A circular doubly linked list.
 */
typedef struct {
    dlst_item_t* head; /**< The head of the list */
} dlst_t;

/**
 * @brief Initializes a doubly linked list.
 */
__STATIC_FORCEINLINE void dlst_init(dlst_t* list)
{
    list->head = NULL;
}

/**
 * @brief Adds an element to the end of a list.
 * @param list The list.
 * @param item The element to add.
 */
void dlst_append(dlst_t* list, dlst_item_t* item);

/**
 * @brief Adds an element to the beginning of the list.
 * @param list The list.
 * @param item The element to add.
 */
void dlst_prepend(dlst_t* list, dlst_item_t* item);

/**
 * @brief Inserts an element just after another in a list.
 * @param pred The element after which the new element must be added.
 * @param item The element to add.
 */
void dlst_insert(dlst_item_t* pred, dlst_item_t* item);

/**
 * @brief Removes an element from a list.
 * @param list The list.
 * @param item The element of the list to remove.
 *
 * Note that this routine assumes the specified element is contained in the
 * list.
 */
void dlst_remove(dlst_t* list, dlst_item_t* item);

/**
 * @brief Rotates a list.
 * @param list The list to rotate, assumed non-empty.
 *
 * Each element of the list is shifted by one position towards the head of the
 * list. The head of the list therefore becomes the tail.
 *
 * This is equivalent, although significantly more efficient, to removing the
 * head from the list and then adding it to the end.
 */
__STATIC_FORCEINLINE void dlst_rotate(dlst_t* list)
{
    ASSERT(list != NULL);
    ASSERT(list->head != NULL);
    list->head = list->head->next;
}

/**
 * @brief Deletes all elements from a list.
 * @param list The list.
 */
__STATIC_FORCEINLINE void dlst_clear(dlst_t* list)
{
    ASSERT(list != NULL);
    list->head = NULL;
}

/**
 * @brief Returns the tail of a list.
 * @return The tail of the queue or NULL if the queue is empty.
 */
__STATIC_FORCEINLINE dlst_item_t* dlst_get_tail(dlst_t const* list)
{
    ASSERT(list != NULL);

    if (list->head == NULL) {
        return NULL;
    }

    return list->head->prev;
}

/**
 * @brief Indicates whether a given list is empty.
 * @return true if the list is empty, false otherwise.
 */
__STATIC_FORCEINLINE bool dlst_is_empty(dlst_t const* list)
{
    ASSERT(list != NULL);
    return list->head == NULL;
}

/**
 * @brief Indicates whether an item in a list has a successor.
 * @param item The item, assumed to be contained in a list.
 * @return true if the item has a successor, false otherwise.
 *
 * Since the list is circular, an item in a list has a successor iff it is not
 * the only item in the list.
 */
__STATIC_FORCEINLINE bool dlst_has_next(dlst_item_t const* item)
{
    ASSERT(item != NULL);
    ASSERT(item->next != NULL);
    return item->next != item;
}

#endif /* KERNEL_DLST_H */
