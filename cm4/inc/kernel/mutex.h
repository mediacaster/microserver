/**
 * @file
 * @brief Mutual exclusion lock.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_MUTEX_H
#define KERNEL_MUTEX_H

#include <stdint.h>
#include "sched.h"
#include "dlst.h"

/**
 * @struct mutex_t
 * @brief Non-recursive mutex.
 */
typedef struct {
    task_t* owner;
    dlst_t wait_queue;
} mutex_t;

/**
 * @struct rmutex_t
 * @brief Recursive mutex.
 */
typedef struct {
    mutex_t wrapped;
    uint32_t nesting;
} rmutex_t;

void mutex_init(mutex_t* mutex);

void rmutex_init(rmutex_t* mutex);

uint32_t mutex_lock(mutex_t* mutex, uint32_t timeout);

void mutex_unlock(mutex_t* mutex);

uint32_t rmutex_lock(rmutex_t* mutex, uint32_t timeout);

void rmutex_unlock(rmutex_t* mutex);

#endif /* KERNEL_MUTEX_H */
