/**
 * @file
 * @brief Fixed-priority preemptive scheduler for ARMv7-M processors.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_SCHED_H
#define KERNEL_SCHED_H

#include <stddef.h>
#include <stdint.h>
#include "kernel.h"
#include "task.h"
#include "dlst.h"

/**
 * @brief Creates and starts a new task.
 * @param task The task control block to initialize.
 * @param func The task routine.
 * @param priority The task priority, lower numeric value means higher priority.
 * @param stack_buf A buffer to use as stack for the task.
 * @param stack_size The size of the task stack, in words.
 *
 * The specified task control block is assumed not to have been already
 * initialized.
 *
 * This routine can be called even if the scheduler has not yet been started. In
 * that case, the task will be started as soon as kernel_start() is invoked.
 *
 * This routine can safely be called from an exception handler.
 */
void sched_create_task(task_t* task, task_func_t func, uint8_t priority, uint32_t* stack_buf,
                       size_t stack_size);

/**
 * @brief Requests a context-switch.
 *
 * If there is at least one other task having the same priority as the current
 * task, the current task is placed at the end of the run queue for its priority
 * and a context-switch is performed.
 *
 * Otherwise, if there is not task having the same priority as the current task
 * calling this routine has no effect.
 */
void sched_yield(void);

/**
 * @brief Get the currently running task.
 * @return The currently running task.
 *
 * Note that this routine is declared with const attribute even if it does not
 * always returns the same value, since repeated calls in a single task is
 * guaranteed to give the exact same result. The compiler can therefore safely
 * optimize out subsequent calls in a given routine. This is particularly useful
 * when the compiler inlines a subroutine calling sched_get_cur_task() in a
 * routine having already performed that call.
 */
__attribute__((const))
task_t* sched_get_cur_task(void);

/**
 * @brief Waits until this task receive an event with a given timeout.
 * @param event_value Set to the task event value on success. Can be NULL.
 * @param timeout The timeout, in kernel ticks.
 * @return 0 on success, ERR_TIMEOUT on timeout.
 *
 * If the task event value is non-zero, meaning at least one event is pending
 * this routine returns immediately, otherwise the task is blocked until an
 * event is received or the timeout expires. If the timeout is zero this routine
 * returns immediately.
 *
 * On success, if event_value is not NULL the task event value is written to
 * *event_value before being cleared.
 */
uint32_t sched_wait_event(uint32_t* event_value, uint32_t timeout);

/**
 * @brief Sends an event to a given task.
 * @param task The task.
 * @param event The event value. Must not be zero.
 *
 * The specified task event value is set to the result of the bitwise OR
 * operation between the task event value and the specified event value.
 *
 * This routine can safely be called from an exception handler.
 */
void sched_send_event(task_t* task, uint32_t event);

/**
 * @brief Makes the calling task sleep for a given number of ticks.
 * @param ticks The number of ticks (< 0xFFFFFFFF).
 *
 * If the specified number of ticks is zero this routine returns immediately.
 */
void sched_sleep(uint32_t ticks);

/**
 * @brief Makes the calling task sleep until a given point in time.
 * @param ref_time A reference time point, updated with the time at which the
 *                 task should wake up. Must not be in the future.
 * @param inc_ticks The number of ticks the task must sleep from the reference
 *                  time point (< 0xFFFFFFFF).
 *
 * This routine makes the current task sleeps until the specified number of
 * ticks have elapsed since the reference time point. If the time at which the
 * task should be waken up has already been reached, this routine returns
 * immediately.
 *
 * This routine is particularly intended to execute a particular action
 * periodically without running the risk of a time drift. To do so, the
 * specified number of ticks should be the execution period and the reference
 * time point should be initially set to the time the action is executed for the
 * first time, then to the next time the action should be performed, which is
 * done automatically by this routine.
 */
void sched_sleep_until(uint64_t* ref_time, uint32_t inc_ticks);

#endif /* KERNEL_SCHED_H */
