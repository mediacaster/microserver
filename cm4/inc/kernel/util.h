/**
 * @file
 * @brief Miscellaneous utility macros.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_UTIL_H
#define KERNEL_UTIL_H

#include <stdint.h>

/**
 * @brief Returns the smaller of two given numbers.
 * @param x The first number.
 * @param y The second number.
 * @return x if x < y, y otherwise.
 */
#define MIN(x, y) ((x) < (y) ? (x) : (y))

/**
 * @brief Returns the larger of two given numbers.
 * @param x The first number.
 * @param y The second number.
 * @return x if x > y, y otherwise.
 */
#define MAX(x, y) ((x) > (y) ? (x) : (y))

/**
 * @brief Clamps a value between an lower and upper limit.
 * @param x The value to clamp.
 * @param min The lower limit.
 * @param max The upper limit.
 * @return min if x < min, max if x > max, x otherwise.
 */
#define CLAMP(x, min, max) (MIN(MAX(x, min), max))

/**
 * @brief Performs an unsigned integer division, rounding up the result.
 * @param a The dividend (integer, >= 0).
 * @param b The divisor (integer, > 0).
 * @return CEIL(a / b), with CEIL the ceiling function.
 */
#define UDIV_UP(a, b) (((a) + ((b) >> 1u)) / (b))

/**
 * @brief Rounds up an integer to the next multiple of a given power of two.
 * @param a The value to round up (integer, >= 0, < 2^32).
 * @param s The alignement (integer, power of two).
 * @return The specified value rounded up according to the given alignement.
 */
#define ALIGN(a, s) (((uint32_t) (a) + ((s) - 1u)) & ~((s) - 1u))

/**
 * @brief Indicates whether an integer is a power of two.
 * @param a The value (integer, >= 0).
 * @return true if the specified value is a power of two, false otherwise.
 */
#define IS_POW2(a) (((a) > 0) && (((a) & ((a) - 1u)) == 0u))

/**
 * @brief Obtains a pointer to the structure instance containing a given field.
 * @param ptr A pointer to the field in the structure instance.
 * @param type The name of the structure.
 * @param field The name of the field.
 * @return A pointer to the structure instance containing the specified field.
 */
#define CONTAINER_OF(ptr, type, field) ((type*) ((uint8_t*) (ptr) - offsetof(type, field)))

/**
 * @brief Indicates to the compiler a given expression is expected to be true.
 * @param x The boolean expression.
 * @return The result of the specified boolean expression.
 */
#define LIKELY(x) __builtin_expect(x, 1)

/**
 * @brief Indicates to the compiler a given expression is expected to be false.
 * @param x The boolean expression.
 * @return The result of the specified boolean expression.
 */
#define UNLIKELY(x) __builtin_expect(x, 0)

/**
 * @brief Indicates a given object is deliberately not used.
 * @param x The unused object.
 */
#define UNUSED(x) (void) (x)

#endif /* KERNEL_UTIL_H */
