/**
 * @file
 * @brief Thread-safe blocking queue.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_QUEUE_H
#define KERNEL_QUEUE_H

#include <stddef.h>
#include <stdint.h>
#include "sched.h"
#include "sem.h"

typedef struct {
    uint32_t* buffer;
    size_t head;
    size_t tail;
    size_t item_size;
    size_t len;
    sem_t poll_sem;
    sem_t offer_sem;
} queue_t;

typedef struct {
    uint32_t* buffer;
    size_t head;
    size_t tail;
    size_t free_items;
    size_t item_size;
    size_t len;
    task_t* waiting_task;
} fqueue_t;

/**
 * @brief Initializes a new queue.
 * @param queue The queue to initialize.
 * @param buffer The queue buffer, where items will be stored.
 * @param item_size The size, in words, of the items.
 * @param len The capacity of the queue, in number of items. Must be a power of
 *            two.
 *
 * This function must be called before the queue can be used.
 */
void queue_init(queue_t* queue, uint32_t* buffer, size_t item_size, size_t len);

uint32_t queue_poll(queue_t* queue, uint32_t* dest, uint32_t timeout);

uint32_t queue_offer(queue_t* queue, uint32_t const* item, uint32_t timeout);

void fqueue_init(fqueue_t* queue, uint32_t* buffer, size_t item_size, size_t len);

uint32_t fqueue_poll(fqueue_t* queue, uint32_t* dest, uint32_t timeout);

uint32_t fqueue_offer(fqueue_t* queue, uint32_t const* item, uint32_t timeout);

#endif /* KERNEL_QUEUE_H */
