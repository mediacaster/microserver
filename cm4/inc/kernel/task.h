/**
 * @file
 * @brief Kernel task.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_TASK_H
#define KERNEL_TASK_H

#include <stdint.h>
#include "kernel.h"
#include "dlst.h"

/**
 * @brief Task routine pointer.
 */
typedef void (*task_func_t)(void);

/**
 * @struct
 * @brief Task control block.
 */
typedef struct task {
    uint32_t* sp;           /* Task stack pointer, must be the first member */
    task_func_t func;       /* Task routine */
    dlst_item_t sched_item; /* Item of the ready/wait queue in which the task is currently contained, if any */
    dlst_t* wait_queue;     /* Wait queue in which the task is currently contained, if any */

#ifndef KERNEL_NO_TIME
    dlst_item_t timeout_item;     /**< Item of the timeout queue in which the task is currently contained, if any */
    dlst_t* owning_timeout_queue; /**< Queue in which this task is contained when waiting with timeout */
    uint32_t waking_tick;         /**< Tick at which the task should wake up, when waiting with timeout */
#endif

    uint32_t event_value;  /**< Bitset containing the pending user events */
    uint8_t priority;      /**< Current task priority, lower numeric value means higher priority */
    uint8_t base_priority; /**< Base task priority */
    uint8_t waited_event;  /**< Waited event when task is blocked, @see task_event_t */
} task_t;

#endif /* KERNEL_TASK_H */
