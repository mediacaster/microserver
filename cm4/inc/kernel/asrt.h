/**
 * @file
 * @brief Assertion macro.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_ASRT_H
#define KERNEL_ASRT_H

#include <stdint.h>

#ifdef DEBUG
#define ASSERT(x) ((x) ? (void) 0 : asrt_failed(__FILE__, __LINE__))

void asrt_failed(char const* file, uint32_t line);
#else
#define ASSERT(x) ((void) sizeof(x))
#endif

#define BUILD_ASSERT(x) _Static_assert(x, "")

#endif /* ASRT_H */
