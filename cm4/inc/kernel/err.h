/**
 * @file
 * @brief Error values.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_ERR_H
#define KERNEL_ERR_H

/**
 * @enum
 * @brief The different types of error.
 */
typedef enum {
    ERR_NONE = 0, /* No error */
    ERR_FAILURE,  /* Generic failure */
    ERR_TIMEOUT   /* Timeout error */
} err_t;

#endif /* KERNEL_ERR_H */
