/**
 * @file
 * @brief Kernel common definitions.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_H
#define KERNEL_H

#include <stddef.h>
#include <stdint.h>
#include <kernel_cfg.h>
#include "err.h"
#include "asrt.h"
#include "util.h"

#if SCHED_PRIO_COUNT > 32u
#error "Invalid kernel configuration: SCHED_PRIO_COUNT"
#endif

#if KERNEL_MAX_SYSCALL_IRQ_PRIO < 1u
#error "Invalid kernel configuration: KERNEL_MAX_SYSCALL_IRQ_PRIO"
#endif

/** Infinite timeout */
#define KERNEL_WAIT_FOREVER 0xFFFFFFFFu

/**
 * @brief Initializes the kernel and starts the first task.
 *
 * This routine performs the necessary initialization for application tasks to
 * be able to run then starts the scheduler and jumps to the first task.
 *
 * Kernel objects can be initialized and tasks can be registered with the
 * scheduler before calling this routine. A least one task must have been
 * registered at the time this routine is called.
 *
 * This routines never returns and must *not* be called multiple times.
 */
__NO_RETURN void kernel_start(void);

/**
 * @brief Enters a critical section.
 *
 * All exceptions having a priority lower or equal to (numerically higher or
 * equal to) KERNEL_MAX_SYSCALL_IRQ_PRIO are masked until the end of the
 * critical section.
 *
 * Critical sections are allowed to nest.
 *
 * Kernel blocking routines must *not* be called in a critical section.
 *
 * This routine can be used in exception handlers.
 */
void kernel_enter_critical(void);

/**
 * @brief Exits a critical section.
 *
 * This routine can be used in exception handlers.
 */
void kernel_exit_critical(void);

/**
 * @brief Returns the kernel uptime.
 * @return The kernel uptime, in ticks.
 *
 * This routine can safely be called from an exception handler.
 */
uint64_t kernel_get_time(void);

/**
 * @brief Returns the kernel current tick.
 * @return The kernel current tick.
 *
 * This routine can safely be called from an exception handler.
 */
uint32_t kernel_get_tick(void);

/**
 * @brief Converts a specified number of microseconds to ticks.
 * @param us A value, in microseconds.
 * @return The specified value converted in ticks, rounded up.
 */
__STATIC_FORCEINLINE uint32_t kernel_us_to_ticks(uint32_t us)
{
    return UDIV_UP(us, (1000u * 1000u) / KERNEL_TICK_RATE_HZ);
}

/**
 * @brief Converts a specified number of milliseconds to ticks.
 * @param ms A value, in milliseconds.
 * @return The specified value converted in ticks, rounded up.
 */
__STATIC_FORCEINLINE uint32_t kernel_ms_to_ticks(uint32_t ms)
{
#if KERNEL_TICK_RATE_HZ <= 1000u
    return UDIV_UP(ms, 1000u / KERNEL_TICK_RATE_HZ);
#else
    ASSERT(ms <= (0xFFFFFFFFU / 1000u));
    return kernel_us_to_ticks(ms * 1000u);
#endif
}

#endif /* KERNEL_H */
