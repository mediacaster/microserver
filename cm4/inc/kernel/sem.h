/**
 * @file
 * @brief Counting semaphore.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_SEM_H
#define KERNEL_SEM_H

#include <stdint.h>
#include "dlst.h"

typedef struct {
    uint32_t count;
    dlst_t wait_queue;
} sem_t;

void sem_init(sem_t* sem, uint32_t init_count);

uint32_t sem_take(sem_t* sem, uint32_t timeout);

void sem_give(sem_t* sem);

void sem_give_ceil(sem_t* sem, uint32_t ceiling);

#endif /* KERNEL_SEM_H */
