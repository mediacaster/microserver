/**
 * @file
 * @brief Kernel configuration file.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_CFG_H
#define KERNEL_CFG_H

#include <stm32h7xx.h>

/**
 * The frequency of the CPU clock, in Hz.
 */
#define KERNEL_CPU_CLOCK_HZ (200u * 1000u * 1000u)

/**
 * The frequency of the kernel clock, in Hz.
 */
#define KERNEL_TICK_RATE_HZ 1000u

/**
 * When defined, kernel timing features are disabled. Kernel does not provide
 * uptime, tasks are not able to sleep and kernel routines accepting a timeout
 * value can only be non-blocking or blocking with infinite waiting period.
 * Specifying a timeout value different from zero and KERNEL_WAIT_FOREVER will
 * result in undefined behavior.
 *
 * Note that even if KERNEL_NO_TIME is defined, time slicing can still be
 * enabled using SCHED_USE_TIME_SLICING.
 *
 * However, when defined and time slicing is not enabled, a kernel clock is not
 * required and SysTick timer will not be used.  This reduces kernel code size
 * and avoid SysTick exception to periodically interrupt the application.
 */
#define KERNEL_NO_TIME

/**
 * When defined, FPU state is not considered during context switches. Therefore
 * the FPU must not be used by the application.
 *
 * Otherwise, FPU is enabled during kernel initialization and at each context
 * switch, the scheduler checks if the FPU registers must be saved and restored
 * depending on whether or not the FPU was used.
 */
#define KERNEL_NO_FPU

/**
 * The maximum priority that an interrupt calling kernel routines can have.
 *
 * Interrupts having a priority higher than the specified value cannot invoke
 * any routine of the kernel.
 *
 * When entering a critical section, all interrupts having a priority lower than
 * or equal to the specified priority are masked until the end of the critical
 * section.
 */
#define KERNEL_MAX_SYSCALL_IRQ_PRIO 8u

/**
 * The number of priority levels available to the tasks.
 *
 * Must not be greater than 32.
 */
#define SCHED_PRIO_COUNT 4u

/**
 * When defined, time slicing is used to schedule tasks with equal priority. The
 * time slice is equal to the kernel clock period.
 *
 * Otherwise, tasks with equal priority are scheduled cooperatively.
 */
/* #define SCHED_USE_TIME_SLICING */

#endif /* KERNEL_CFG_H */
