/**
 * @file
 * @brief Cortex-M4 application main routine.
 * @author Thomas Altenbach
 */

#include <stdbool.h>
#include <stm32h7xx.h>
#include "rcc.h"
#include "sysnotif.h"

/**
 * @brief Stops D2 domain and waits until Core 1 send a wake-up event.
 */
static void stop_d2_domain(void);

void main(void)
{
    /* Initialize inter-core notifications */
    sysnotif_init();
    /* Enter STOP mode */
    stop_d2_domain();
    /* Update value of system core clock */
    SystemCoreClockUpdate();

    /* Enable ART accelerator */
    rcc_ahb1_enable_clock(RCC_AHB1ENR_ARTEN);
    ART->CTR |= (FLASH_BANK2_BASE & 0xFFF00000u) >> 12u;
    ART->CTR |= ART_CTR_EN;

    // TODO: implement

    rcc_ahb4_enable_clock(RCC_AHB4ENR_GPIOIEN);

    GPIOI->MODER = (GPIOI->MODER & ~GPIO_MODER_MODE13) | GPIO_MODER_MODE13_0;

    while (true) {
        GPIOI->ODR ^= GPIO_ODR_OD13;
        for (uint32_t i = 0u; i < 5000000u; ++i) {}
    }
}

static void stop_d2_domain(void)
{
    /* Clear any pending event */
    __SEV();
    __WFE();

    PWR->CR1 &= ~PWR_CR1_LPDS;
    PWR->CPUCR &= ~PWR_CPUCR_PDDS_D2;

    SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
    sysnotif_wait_no_kernel(SYSNOTIF_SYSTEM_READY_MASK);
    SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;
}
