/**
 * @file
 * @brief Fixed-priority preemptive scheduler for ARMv7-M processors.
 * @author Thomas Altenbach
 *
 * The routines declared in this file are internal kernel routines and must
 * *not* be called from the application code.
 */

#ifndef KERNEL_SCHED_H_INTERNAL
#define KERNEL_SCHED_H_INTERNAL

#include <stdbool.h>
#include <kernel/sched.h>

/**
 * @brief Starts the scheduler.
 *
 * This routine starts the first task by triggering PendSV exception.
 *  - If PendSV is masked when this routine is called, this routine will return
 *    and the first task will start as soon as PendSV as the opportunity to
 *    execute.
 *  - Otherwise, this routine may or may not return, since some instructions may
 *    be executed by the CPU between the time the exception is trigerred and
 *    and the time it is handled.
 *
 * At least one task must have been registered before calling this routine.
 *
 * This routine must *not* be called multiple times.
 *
 * This routine must *not* be called from an exception handler.
 */
void sched_start(void);

/**
 * @brief Changes temporarly the priority of a task.
 * @param task The task.
 * @param new_prio The new active priority.
 *
 * Only the active priority of the task is changed, i.e. the priority taken into
 * account by the scheduler. The base priority is not altered and can be
 * restored at any time by calling sched_restore_base_priority().
 *
 * This routine must be called in a critical section.
 */
void sched_set_active_priority(task_t* task, uint8_t new_prio);

/**
 * @brief Restores the base priority of a task.
 * @param task The task.
 *
 * This routine must be called in a critical section.
 */
void sched_restore_base_priority(task_t* task);

/**
 * @brief Waits until a notification is received with a relative timeout.
 * @param timeout The timeout, in kernel ticks.
 *
 * This routine must be called in a critical section.
 */
void sched_wait(uint32_t timeout);

/**
 * @brief Waits until a notification is received with an absolute timeout.
 * @param waking_time The time point at which the timeout expires. UINT64_MAX
 *                    for infinite timeout.
 * @return 0 if the timeout has not already expired and the task was blocked,
 *         ERR_TIMEOUT otherwise.
 *
 * This routine must be called in a critical section.
 */
uint32_t sched_wait_until(uint64_t waking_time);

/**
 * @brief Waits with a relative timeout in a wait queue until a notification is
 *        received.
 * @param wait_queue The wait queue.
 * @param timeout The timeout, in kernel ticks.
 *
 * The queue is assumed to be sorted on descending priority order. The current
 * task is inserted into the queue so that the order is preserved.
 *
 * This routine must be called in a critical section. The critical section can
 * only be nested if the timeout is zero.
 */
void sched_wait_in_queue(dlst_t* wait_queue, uint32_t timeout);

/**
 * @brief Waits with an absolute timeout in a wait queue until a notification is
 *        received.
 * @param wait_queue The wait queue.
 * @param waking_time The time point at which the timeout expires. UINT64_MAX
 *                    for infinite timeout.
 * @return 0 if the timeout has not already expired and the task was blocked,
 *         ERR_TIMEOUT otherwise.
 *
 * The queue is assumed to be sorted on descending priority order. The current
 * task is inserted into the queue so that the order is preserved.
 *
 * This routine must be called in a non-nested critical section.
 */
uint32_t sched_wait_in_queue_until(dlst_t* wait_queue, uint64_t waking_time);

/**
 * @brief Notifies a given task.
 * @param task The task.
 *
 * If the task is currently waiting for a notification, the task is waken up.
 *
 * This routine must be called in a critical section.
 */
void sched_notify(task_t* task);

/**
 * @brief Notifies the next task in a wait queue.
 * @param wait_queue The wait queue.
 * @return The task that was notified, if any, NULL otherwise.
 *
 * If the queue is not empty, the next task in the queue is waken up.
 *
 * The queue is assumed to be sorted in descending priority order.
 *
 * This routine must be called in a critical section.
 */
task_t* sched_notify_next(dlst_t* wait_queue);

/**
 * @brief Indicates if a context switch is currently possible.
 * @return true if a context switch can currently be performed, false otherwise.
 *
 * A context switch can currently be performed if and only if the PendSV
 * exception can be executed at the time this routine is called, that is to say
 * if and only if this routine is not executed from an exception handler and
 * that exceptions are not currently disabled.
 */
bool sched_can_switch_context(void);

/**
 * @brief Notifies the scheduler that a kernel tick has occured.
 * @param tick The new tick value.
 *
 * This routine must be called once and only once from the kernel timer
 * exception handler at each tick. It must not be called within a critical
 * section.
 *
 * If KERNEL_NO_TIME is defined, the specified tick value is not considered.
 */
void sched_notify_tick(uint32_t tick);

#endif /* KERNEL_SCHED_H_INTERNAL */
