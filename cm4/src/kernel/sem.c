/**
 * @file
 * @brief Counting semaphore.
 * @author Thomas Altenbach
 */

#include "sem.h"
#include "kernel.h"
#include "sched.h"
#include "dlst.h"

static void give_in_critical(sem_t* sem);

void sem_init(sem_t* sem, uint32_t init_count)
{
    sem->count = init_count;
    dlst_init(&sem->wait_queue);
}

uint32_t sem_take(sem_t* sem, uint32_t timeout)
{
    ASSERT(sem != NULL);
    ASSERT(timeout == 0u || sched_can_switch_context());

    kernel_enter_critical();

    if (sem->count != 0u) {
        sem->count--;
        kernel_exit_critical();
        return 0u;
    }

    if (timeout == 0u) {
        kernel_exit_critical();
        return ERR_TIMEOUT;
    }

    uint64_t timeout_end = kernel_get_timeout_end(timeout);
    dlst_t* wait_queue = &sem->wait_queue;

    do {
        uint32_t ret = sched_wait_in_queue_until(wait_queue, timeout_end);

        if (ret != 0u) {
            kernel_exit_critical();
            return ret;
        }
    } while (sem->count == 0u);

    sem->count--;
    kernel_exit_critical();

    return 0u;
}

void sem_give(sem_t* sem)
{
    ASSERT(sem != NULL);
    ASSERT(sem->count != 0xFFFFFFFFu);

    kernel_enter_critical();
    give_in_critical(sem);
    kernel_exit_critical();
}

void sem_give_ceil(sem_t* sem, uint32_t ceiling)
{
    ASSERT(sem != NULL);

    kernel_enter_critical();

    if (sem->count < ceiling) {
        give_in_critical(sem);
    }

    kernel_exit_critical();
}

static void give_in_critical(sem_t* sem)
{
    ASSERT(kernel_is_in_critical_section());

    sem->count++;
    sched_notify_next(&sem->wait_queue);
}
