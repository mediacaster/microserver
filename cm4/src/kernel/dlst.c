/**
 * @file
 * @brief Circular doubly linked list, optimized for use in scheduler.
 * @author Thomas Altenbach
 */

#include "dlst.h"

void dlst_prepend(dlst_t* list, dlst_item_t* item)
{
    ASSERT(list != NULL);
    ASSERT(item != NULL);

    dlst_append(list, item);
    list->head = item;
}

void dlst_append(dlst_t* list, dlst_item_t* item)
{
    ASSERT(list != NULL);
    ASSERT(item != NULL);

    dlst_item_t* head = list->head;

    if (head == NULL) {
        item->next = item;
        item->prev = item;
        list->head = item;
    } else {
        item->prev = head->prev;
        item->next = head;
        head->prev->next = item;
        head->prev = item;
    }
}

void dlst_insert(dlst_item_t* pred, dlst_item_t* item)
{
    ASSERT(pred != NULL);
    ASSERT(item != NULL);

    item->prev = pred;
    item->next = pred->next;
    pred->next->prev = item;
    pred->next = item;
}

void dlst_remove(dlst_t* list, dlst_item_t* item)
{
    ASSERT(list != NULL);
    ASSERT(item != NULL);

    if (item->next == item) {
        list->head = NULL;
    } else if (list->head == item) {
        list->head = item->next;
    }

    item->prev->next = item->next;
    item->next->prev = item->prev;
}
