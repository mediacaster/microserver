/**
 * @file
 * @brief Counting semaphore.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_SEM_H_INTERNAL
#define KERNEL_SEM_H_INTERNAL

#include <kernel/sem.h>

#endif /* KERNEL_SEM_H_INTERNAL */
