/**
 * @file
 * @brief Kernel global routines and definitions.
 * @author Thomas Altenbach
 */

#include "kernel.h"
#include "sched.h"

/** CPACR mask for enabling FPU with full access */
#define CPACR_FPU_FULL_ACCESS (0b11u << 20u)

#if !defined(KERNEL_NO_TIME) || defined(SCHED_USE_TIME_SLICING)
/** Defined when SysTick timer must be used to generate kernel ticks */
#define USE_SYSTICK
#endif

/** Nesting counter for critical sections */
static uint32_t g_critical_nesting;

#ifndef KERNEL_NO_TIME
/** Kernel uptime, in ticks */
static uint64_t g_time;
#endif

#ifdef USE_SYSTICK
/**
 * @brief Initializes and starts kernel timer.
 */
static void setup_systick(void);
#endif

void kernel_start(void)
{
    ASSERT(sched_can_switch_context());

#ifndef KERNEL_NO_FPU
    /* Enable FPU */
    SCB->CPACR |= CPACR_FPU_FULL_ACCESS;

    /*
     * Ensure CONTROL bit is set on execution of a floating-point instruction
     * and that to lazy state preservation for floating-point context is
     * enabled.
     */
    FPU->FPCCR |= FPU_FPCCR_ASPEN_Msk | FPU_FPCCR_LSPEN_Msk;

    /*
     * Clear the bit indicating that the FPU is in use in case the latter was
     * used before the scheduler has been started.
     */
    __set_CONTROL(0x00u);
#endif

    /*
     * Start kernel timer and scheduler.
     *
     * Since this routine will never return to the caller, MSP is reset to start
     * of stack to avoid wasting the currently used stack memory.
     *
     * This part is done in assembly to ensure stack is not used by this routine
     * after MSP is cleared.
     */
    __asm volatile (
        "    ldr r0, =%[vtor]        \n" /* Get vector table address */
        "    ldr r0, [r0]            \n"
        "    ldr r0, [r0]            \n" /* Get initial MSP value */
        "    msr msp, r0             \n" /* Reset MSP to start of stack */
#ifdef USE_SYSTICK
        "    mov r1, %[basepri]      \n" /* Enter critical section */
        "    msr basepri, r1         \n"
#if defined(__CM7_REV) && __CM7_REV == 0x0001u
        "    ands r0, r0             \n" /* Workaround for Cortex-M7 r0p1 erratum n°837070 */
#endif
        "    bl %[setup_systick]     \n" /* Configure and start kernel timer */
#endif /* USE_SYSTICK */
        "    bl %[sched_start]       \n" /* Start the scheduler */
#ifdef USE_SYSTICK
        "    movs r0, #0             \n" /* Exit critical section */
        "    msr basepri, r0         \n"
#endif
        "    b .                     \n" /* Wait for PendSV to be executed and ensure no return */
        ::
        [vtor] "i" (&SCB->VTOR),
#ifdef USE_SYSTICK
        [basepri] "i" (KERNEL_CRITICAL_SEC_BASEPRI),
        [setup_systick] "i" (setup_systick),
#endif
        [sched_start] "i" (sched_start)
        : "memory"
    );

    __builtin_unreachable();
}

void kernel_enter_critical(void)
{
    kernel_enter_critical_no_nesting();
    ++g_critical_nesting;

    /* Ensure no overflow occurred */
    ASSERT(g_critical_nesting > 0u);
}

void kernel_exit_critical(void)
{
    ASSERT(g_critical_nesting > 0u);

    __COMPILER_BARRIER();

    if (--g_critical_nesting == 0u) {
        kernel_exit_critical_no_nesting();
    }
}

#ifndef KERNEL_NO_TIME
uint64_t kernel_get_time(void)
{
    uint64_t cur_time;

    /*
     * Reading a 64-bit value is not a single-copy atomic operation in ARMv7-M
     * processors. Here, that operation can however be performed safely without
     * using a critical section by taking advantage of LDRD instruction
     * properties. Indeed, if an exception arrives during the execution of LDRD,
     * the instruction execution will be abandoned and restarted from scratch
     * after the exception has been handled. Since g_time is only modified in
     * an exception handler, using LDRD ensure therefore a consistent 64-bit
     * value is obtained, even if g_time is increment concurrently.
     *
     * Assembly is used here to make sure the load is performed using LDRD.
     */
    __asm("ldrd %0, [%1]" : "=r" (cur_time) : "r" (&g_time));

    return cur_time;
}

uint32_t kernel_get_tick(void)
{
    return (uint32_t) g_time;
}

uint64_t kernel_get_timeout_end(uint32_t timeout)
{
    if (timeout == KERNEL_WAIT_FOREVER) {
        return UINT64_MAX;
    }

    return kernel_get_time() + timeout;
}
#endif

bool kernel_is_in_critical_section(void)
{
    if (__get_PRIMASK() != 0u) {
        return true;
    }

    uint32_t basepri = __get_BASEPRI();

    return basepri != 0u && basepri <= KERNEL_CRITICAL_SEC_BASEPRI;
}

bool kernel_is_in_nested_critical_section(void)
{
    return g_critical_nesting > 0u;
}

bool kernel_is_in_non_nested_critical_section(void)
{
    bool critical_section = kernel_is_in_critical_section();

    return critical_section && g_critical_nesting == 0u;
}

#ifdef USE_SYSTICK
/**
 * @brief SysTick exception handler.
 */
void SysTick_Handler(void)
{
    uint32_t new_tick;

#ifdef KERNEL_NO_TIME
    new_tick = 0u;
#else
    /* Increment time atomically (64-bit store) */
    kernel_enter_critical_no_nesting();
    ++g_time;
    new_tick = (uint32_t) g_time;
    kernel_exit_critical_no_nesting();
#endif

    sched_notify_tick(new_tick);
}

static void setup_systick(void)
{
    /*
     * Set SysTick exception to the lowest priority.
     *
     * Lowest priority means that ticks can be delayed, or in extreme situations
     * lost, in case the tick occurs during a critical section or the handling
     * of another exception. However it is prefered to a higher priority since:
     *  - A tick can only be lost the handling of the SysTick exception is
     *    prevented for a duration higher than the tick period. However, the
     *    duration of an exception or critical section should be significantly
     *    lower than the tick period, or the application has a serious design
     *    issue .
     *  - The previous point implies an exception or critical section should not
     *    rely on the evolution of the tick value. Therefore it is not an issue
     *    if no update of the tick value is performed during exceptions or
     *    critical sections.
     *  - Since only code outside critical section or exception should rely on
     *    the evolution of the tick, delaying the update of the tick value is
     *    not an issue because the execution of that code is also delayed, for
     *    the same reason, and the SysTick exception will be executed before it
     *    has the opportunity to run. Therefore, performing the tick update
     *    without delay would have no added value.
     *  - Using lowest priority, the tick value cannot be updated during a
     *    PendSV exception or a critical section, which simplifies the design of
     *    kernel, leading to smaller and faster code.
     */
    NVIC_SetPriority(SysTick_IRQn, 0xFFu);

    /* Configure and start SysTick */
    uint32_t period;
    uint32_t ctrl = SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk;

    /* SysTick is a 24-bit timer, check first if prescaler must be used */
    if (KERNEL_CPU_CLOCK_HZ / KERNEL_TICK_RATE_HZ < 0x01000000u) {
        /* Use directly AHB clock for SysTick */
        ctrl |= SysTick_CTRL_CLKSOURCE_Msk;
        period = KERNEL_CPU_CLOCK_HZ / KERNEL_TICK_RATE_HZ - 1u;
    } else {
        /* Use AHB/8 as clock source */
        period = (KERNEL_CPU_CLOCK_HZ / 8u) / KERNEL_TICK_RATE_HZ - 1u;
    }

    ASSERT(period > 1u);

    SysTick->LOAD = period - 1u;
    SysTick->VAL = 0u;
    SysTick->CTRL = ctrl;
}
#endif /* USE_SYSTICK */
