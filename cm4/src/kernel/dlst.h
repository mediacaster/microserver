/**
 * @file
 * @brief Circular doubly linked list, optimized for use in scheduler.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_DLST_H_INTERNAL
#define KERNEL_DLST_H_INTERNAL

#include <kernel/dlst.h>

#endif /* KERNEL_DLST_H_INTERNAL */
