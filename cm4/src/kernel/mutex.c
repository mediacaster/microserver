/**
 * @file
 * @brief Mutual exclusion lock.
 * @author Thomas Altenbach
 */

#include "mutex.h"
#include "kernel.h"
#include "sched.h"
#include "task.h"
#include "dlst.h"

void mutex_init(mutex_t* mutex)
{
    mutex->owner = NULL;
    dlst_init(&mutex->wait_queue);
}

void rmutex_init(rmutex_t* mutex)
{
    mutex_init(&mutex->wrapped);
    mutex->nesting = 0u;
}

static uint32_t wait_and_take(mutex_t* mutex, uint32_t timeout)
{
    ASSERT(mutex->owner != NULL);
    ASSERT(kernel_is_in_critical_section());

    if (timeout == 0u) {
        return ERR_TIMEOUT;
    }

    task_t* cur_task = sched_get_cur_task();

    /* If owner has lower priority than waiter, raise owner priority */
    if (task_cmp_prio(cur_task, mutex->owner) > 0) {
        sched_set_active_priority(mutex->owner, cur_task->priority);
    }

    /* Wait with timeout until the mutex is taken */
    dlst_t* wait_queue = &mutex->wait_queue;
    sched_wait_in_queue(wait_queue, timeout);

    if (mutex->owner == cur_task) {
        return 0u; /* Success */
    }

    /* Timed out, adjust owner priority */
    task_t* next_waiter = task_from_sched_item(wait_queue->head);
    uint32_t new_prio = mutex->owner->base_priority;

    if (next_waiter != NULL) {
        if (task_cmp_prio_val(next_waiter->priority, new_prio) > 0) {
            new_prio = next_waiter->priority;
        }
    }

    sched_set_active_priority(mutex->owner, new_prio);

    return ERR_TIMEOUT;
}

uint32_t mutex_lock(mutex_t* mutex, uint32_t timeout)
{
    ASSERT(mutex != NULL);
    ASSERT(timeout == 0u || sched_can_switch_context());

    uint32_t ret;
    task_t* cur_task = sched_get_cur_task();

    kernel_enter_critical();

    if (LIKELY(mutex->owner == NULL)) {
        mutex->owner = cur_task;
        ret = 0u;
    } else {
        ret = wait_and_take(mutex, timeout);
    }

    kernel_exit_critical();

    return ret;
}

void mutex_unlock(mutex_t* mutex)
{
    ASSERT(mutex != NULL);
    ASSERT(mutex->owner == sched_get_cur_task());

    task_t* next_owner;

    kernel_enter_critical();

    next_owner = sched_notify_next(&mutex->wait_queue);

    if (next_owner != NULL) {
        sched_restore_base_priority(mutex->owner);
    }

    mutex->owner = next_owner;

    kernel_exit_critical();
}

uint32_t rmutex_lock(rmutex_t* mutex, uint32_t timeout)
{
    ASSERT(mutex != NULL);

    if (mutex->wrapped.owner == sched_get_cur_task()) {
        ++mutex->nesting;
        return 0u;
    }

    return mutex_lock(&mutex->wrapped, timeout);
}

void rmutex_unlock(rmutex_t* mutex)
{
    ASSERT(mutex != NULL);
    ASSERT(mutex->wrapped.owner == sched_get_cur_task());

    if (--mutex->nesting == 0u) {
        mutex_unlock(&mutex->wrapped);
    }
}
