/**
 * @file
 * @brief Mutual exclusion lock.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_MUTEX_H_INTERNAL
#define KERNEL_MUTEX_H_INTERNAL

#include <kernel/mutex.h>

#endif /* KERNEL_MUTEX_H_INTERNAL */
