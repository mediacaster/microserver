/**
 * @file
 * @brief Assertion macro.
 * @author Thomas Altenbach
 */

#include <stdbool.h>
#include "kernel.h"

#ifdef DEBUG
__WEAK void asrt_failed(char const* file, uint32_t line)
{
    UNUSED(file);
    UNUSED(line);

    __disable_irq();

    /* Never return */
    while (true) {
        __asm volatile ("");
    }
}
#endif /* NDEBUG */
