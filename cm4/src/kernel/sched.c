/**
 * @file
 * @brief Fixed-priority preemptive scheduler for ARMv7-M processors.
 * @author Thomas Altenbach
 */

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include "kernel.h"
#include "sched.h"
#include "task.h"
#include "dlst.h"

/** The current task, NULL if no current task */
static task_t* g_cur_task;
/** Queues containing the tasks ready for execution. One queue per priority */
static dlst_t g_ready_queues[SCHED_PRIO_COUNT];
/** Bitset indicating the priorities for which there is at least one ready task */
static uint32_t g_ready_priorities;

#ifndef KERNEL_NO_TIME
/*
 * Timeout queues, containing the tasks waiting for an event with finite
 * timeout. In the queues, tasks are sorted in ascending order according to
 * their waking time, ie. the time point where their timeout expires.
 *
 * To avoid using 64-bit integer for storing the waking time, two queues are
 * used to properly handle tick counter wrap around: one for tasks whose waking
 * time happens before the overflow, and one for the others. The two queues are
 * swapped when the wrap around occurs.
 */
static dlst_t g_timeout_queue_1;
static dlst_t g_timeout_queue_2;

/** Current timeout queue for tasks waking up before next tick overflow */
static dlst_t* g_timeout_queue = &g_timeout_queue_1;
/** Current timeout queue for tasks waking up on or after next tick overflow */
static dlst_t* g_overflow_timeout_queue = &g_timeout_queue_2;
#endif

/**
 * @brief Triggers the execution of the scheduler.
 *
 * This routine can be called in an exception or a critical section. In that
 * case the scheduler will be executed as soon as the PendSV exception handler
 * has the opportunity to run.
 *
 * In case the PendSV exception is not masked when this routine is called, the
 * scheduler can be executed immediately, but note that this routine does not
 * provide any guarantee that the scheduler will actually be invoked straight
 * away. Indeed, some instruction could be executed before the PendSV exception
 * entry. If ensuring the immediate execution of the scheduler is required, an
 * ISB instruction must be added after the call to this routine.
 */
static void invoke_scheduler(void);

/**
 * @brief Chooses the next task to run.
 * @return The next task to run, or NULL if no ready task available.
 *
 * Must be called in a critical section.
 */
static task_t* choose_next_task(void);

/**
 * @brief Gets the highest priority for which there is a ready task.
 * @return The highest priority for which at least one task is ready.
 */
static uint32_t get_highest_ready_prio(void);

/**
 * @brief Registers a task to its associated ready queue.
 * @param task The task. Must not be contained in a ready or wait queue.
 *
 * Must be called in a critical section.
 */
static void register_ready_task(task_t* task);

/**
 * @brief Unregisters a task from its ready queue.
 * @param task The task. Must be contained in a ready queue.
 *
 * Must be called in a critical section.
 */
static void unregister_ready_task(task_t* task);

/**
 * @brief Blocks the current task with relative timeout.
 * @param waited_event The event waited by the task.
 * @param timeout The timeout, in kernel ticks. KERNEL_WAIT_FOREVER for infinite
 *                timeout.
 *
 * If the timeout is zero, this routine returns immediately without blocking the
 * task.
 *
 * Must be called in a critical section.
 */
static void block_current_task(task_evt_t waited_event, uint32_t timeout);

/**
 * @brief Blocks the current with absolute timeout.
 * @param waited_event The event waited by the task.
 * @param waking_time The time point at which the timeout expires. UINT64_MAX
 *                    for infinite timeout.
 * @return 0 on success, ERR_TIMEOUT if the timeout has already expired.
 *
 * If the timeout has already expired, ie. is the past or is the current time,
 * this routine returns immediately without blocking the task.
 *
 * Must be called in a critical section.
 */
static uint32_t block_current_task_until(task_evt_t waited_event, uint64_t waking_time);

/**
 * @brief Unblocks a task.
 * @param task The task to wake up.
 *
 * The scheduler is invoked if necessary.
 *
 * Must be called in a critical section.
 */
static void unblock_task(task_t* task);

/**
 * @brief Unblocks a task and indicates if the scheduler must be invoked.
 * @param task The task to wake up.
 * @return true if the scheduler must be invoked, false otherwise.
 *
 * Must be called in a critical section.
 */
static bool unblock_task_no_yield(task_t* task);

/**
 * @brief Unblocks a task if it was waiting for the specified event.
 * @param task The task to wake up.
 * @param event The event that occurs.
 *
 * If the task is not waiting for the specified event, this routine returns
 * immediately without unblocking the task.
 *
 * The scheduler is invoked if necessary.
 *
 * Must be called in a critical section.
 */
static void try_unblock_task(task_t* task, task_evt_t event);

/**
 * @brief Adds a task to a wait queue.
 * @param wait_queue The wait queue.
 * @param task The task. Must not be contained in a ready or wait queue.
 *
 * Must be called in a critical section.
 */
static void add_task_to_wait_queue(dlst_t* wait_queue, task_t* task);

/**
 * @brief Removes a task from the wait queue it is currently waiting in.
 * @param task The task. Must be contained in a wait queue.
 *
 * Must be called in a critical section.
 */
static void remove_task_from_wait_queue(task_t* task);

#ifndef KERNEL_NO_TIME
/**
 * @brief Adds a task to a timeout queue.
 * @param queue The timeout queue.
 * @param task The task. Must not be already contained in a timeout queue.
 * @param waking_tick The kernel tick at which the timeout expires.
 *
 * Must be called in a critical section.
 */
static void add_to_timeout_queue(dlst_t* queue, task_t* task, uint32_t waking_tick);
#endif

void sched_create_task(task_t* task, task_func_t func, uint8_t priority, uint32_t* stack_buf,
                       size_t stack_size)
{
    ASSERT(task != NULL);
    ASSERT(func != NULL);
    ASSERT(stack_buf != NULL);

    task_init(task, func, priority, stack_buf, stack_size);

    kernel_enter_critical();
    register_ready_task(task);

    if (g_cur_task != NULL && task_cmp_prio(task, g_cur_task) > 0) {
        invoke_scheduler();
    }

    kernel_exit_critical();
}

void sched_yield(void)
{
    if (dlst_has_next(&g_cur_task->sched_item)) {
        return; /* No other task of equal priority */
    }

#ifndef SCHED_USE_TIME_SLICING
    kernel_enter_critical();
    dlst_rotate(&g_ready_queues[g_cur_task->priority]);
    kernel_exit_critical();
#endif

    invoke_scheduler();
    __ISB(); /* Enter PendSV immediately */
}

task_t* sched_get_cur_task(void)
{
    return g_cur_task;
}

uint32_t sched_wait_event(uint32_t* event_value, uint32_t timeout)
{
    ASSERT(timeout == 0u || sched_can_switch_context());

    kernel_enter_critical();

    if (timeout != 0u && g_cur_task->event_value == 0u) {
        block_current_task(TASK_EVT_USER, timeout);
        kernel_exit_critical();

        /* PendSV is executed here */

        kernel_enter_critical();
    }

    uint32_t ret = 0u;

    if (g_cur_task->event_value != 0u) {
        if (event_value != NULL) {
            *event_value = g_cur_task->event_value;
        }

        g_cur_task->event_value = 0u;
    } else {
        ret = ERR_TIMEOUT;
    }

    kernel_exit_critical();

    return ret;
}

void sched_send_event(task_t* task, uint32_t event)
{
    ASSERT(task != NULL);
    ASSERT(event != 0u);

    kernel_enter_critical();
    task->event_value |= event;
    try_unblock_task(task, TASK_EVT_USER);
    kernel_exit_critical();
}

#ifndef KERNEL_NO_TIME
void sched_sleep(uint32_t ticks)
{
    ASSERT(ticks != KERNEL_WAIT_FOREVER);
    ASSERT(sched_can_switch_context());

    kernel_enter_critical();
    block_current_task(TASK_EVT_SLEEP_END, ticks);
    kernel_exit_critical();
}

void sched_sleep_until(uint64_t* ref_time, uint32_t inc_ticks)
{
    ASSERT(ref_time != NULL);
    ASSERT(inc_ticks != KERNEL_WAIT_FOREVER);
    ASSERT(inc_ticks <= UINT64_MAX - *ref_time);
    ASSERT(kernel_get_time() >= *ref_time);
    ASSERT(sched_can_switch_context());

    kernel_enter_critical();

    *ref_time += inc_ticks;
    block_current_task_until(TASK_EVT_SLEEP_END, *ref_time);

    kernel_exit_critical();
}
#endif

void sched_start(void)
{
    ASSERT(g_cur_task == NULL);
    ASSERT(g_ready_priorities != 0u);

    /* Set PendSV exception, performing context switches, to the lowest priority */
    NVIC_SetPriority(PendSV_IRQn, 0xFFu);

    /*
     * PendSV routine assumes there is a current running task, so choose an
     * arbitrary task to be the current task.
     */
    uint32_t queue_ind = get_highest_ready_prio();
    dlst_item_t* task_item = g_ready_queues[queue_ind].head;
    task_t* task = task_from_sched_item(task_item);

    /* Clear task stack (xPSR, PC, LR, R12, R3->R0, R11->R4, EXC_RETURN) */
    task->sp += 17u;

    /*
     * Set current task. At the time g_cur_task becomes non-null, scheduler
     * will be assumed to have been started, ie. PendSV can be executed.
     * Routines executed concurrently to this one can thus invoke the scheduler,
     * and a compiler barrier is therefore needed to ensure this assignment
     * won't be executed before previous instructions.
     */
    __COMPILER_BARRIER();
    g_cur_task = task;

    /* Invoke the scheduler to choose the first task and jump to it */
    invoke_scheduler();
}

void sched_set_active_priority(task_t* task, uint8_t new_prio)
{
    ASSERT(task != NULL);
    ASSERT(new_prio < SCHED_PRIO_COUNT);
    ASSERT(kernel_is_in_critical_section());

    if (new_prio == task->priority) {
        return;
    }

    if (task->waited_event != TASK_EVT_NONE) {
        /* Task is blocked, update priority */
        task->priority = new_prio;

        /* If the task is blocked in a wait queue, re-order the queue */
        if (task->wait_queue != NULL) {
            dlst_t* wait_queue = task->wait_queue;

            remove_task_from_wait_queue(task);
            add_task_to_wait_queue(wait_queue, task);
        }
    } else {
        /* Task is ready, ready queues must be updated */
        unregister_ready_task(task);
        task->priority = new_prio;
        register_ready_task(task);

        /* Invoke scheduler if a context-switch is required */
        if (task == g_cur_task) {
            if (new_prio != get_highest_ready_prio()) {
                invoke_scheduler();
            }
        } else {
            if (task_cmp_prio(task, g_cur_task) > 0) {
                invoke_scheduler();
            }
        }
    }
}

void sched_restore_base_priority(task_t* task)
{
    ASSERT(task != NULL);
    ASSERT(kernel_is_in_critical_section());

    sched_set_active_priority(task, task->base_priority);
}

void sched_wait(uint32_t timeout)
{
    ASSERT(kernel_is_in_critical_section());
    block_current_task(TASK_EVT_NOTIF, timeout);
}

uint32_t sched_wait_until(uint64_t waking_time)
{
    ASSERT(kernel_is_in_critical_section());
    return block_current_task_until(TASK_EVT_NOTIF, waking_time);
}

void sched_wait_in_queue(dlst_t* wait_queue, uint32_t timeout)
{
    ASSERT(wait_queue != NULL);
    ASSERT(kernel_is_in_critical_section());

    if (timeout == 0u) {
        return;
    }

    add_task_to_wait_queue(wait_queue, g_cur_task);
    block_current_task(TASK_EVT_NOTIF, timeout);

    kernel_exit_critical_no_nesting();
    /* Context-switch happens here */
    kernel_enter_critical_no_nesting();
}

uint32_t sched_wait_in_queue_until(dlst_t* wait_queue, uint64_t waking_time)
{
    ASSERT(wait_queue != NULL);
    ASSERT(kernel_is_in_critical_section());

    uint32_t ret = block_current_task_until(TASK_EVT_NOTIF, waking_time);

    if (ret != 0u) {
        return ret;
    }

    add_task_to_wait_queue(wait_queue, g_cur_task);

    kernel_exit_critical_no_nesting();
    /* Context-switch happens here */
    kernel_enter_critical_no_nesting();

    return 0u;
}

void sched_notify(task_t* task)
{
    ASSERT(task != NULL);
    ASSERT(kernel_is_in_critical_section());

    try_unblock_task(task, TASK_EVT_NOTIF);
}

task_t* sched_notify_next(dlst_t* wait_queue)
{
    ASSERT(wait_queue != NULL);
    ASSERT(kernel_is_in_critical_section());

    task_t* task = NULL;

    if (wait_queue->head != NULL) {
        task = task_from_sched_item(wait_queue->head);
        try_unblock_task(task, TASK_EVT_NOTIF);
    }

    return task;
}

bool sched_can_switch_context(void)
{
    if (__get_PRIMASK() != 0u || __get_BASEPRI() != 0) {
        return false; /* PendSV exception is masked */
    }

    if (SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk) {
        return false; /* In an exception handler, PendSV cannot execute */
    }

    return true;
}

#if !defined(KERNEL_NO_TIME) || defined(SCHED_USE_TIME_SLICING)
void sched_notify_tick(uint32_t tick)
{
    ASSERT(!kernel_is_in_critical_section());

    bool yield_required = false;

#ifdef KERNEL_NO_TIME
    UNUSED(tick);
#else
    kernel_enter_critical_no_nesting();

    if (tick == 0u) {
        /*
         * Overflow has occured, all tasks with timeout before overflow should
         * have been unblocked during previous calls to this routine.
         */
        ASSERT(dlst_is_empty(g_timeout_queue));

        /* Swap timeout queues */
        dlst_t* new_overflow_queue = g_timeout_queue;
        g_timeout_queue = g_overflow_timeout_queue;
        g_overflow_timeout_queue = new_overflow_queue;
    }

    dlst_item_t* next_wakeup = g_timeout_queue->head;

    /* Check if tasks must be waken up due to a timeout */
    while (next_wakeup != NULL) {
        task_t* task = task_from_timeout_item(next_wakeup);

        if (task->waking_tick > tick) {
            break; /* No more tasks should be waken up at this tick */
        }

        /* If so, unblock those tasks */
        yield_required = unblock_task_no_yield(task);

        next_wakeup = g_timeout_queue->head;
    }

    kernel_exit_critical_no_nesting();
#endif

#ifdef SCHED_USE_TIME_SLICING
    /*
     * When using time-slicing, a yield must be performed if several tasks share
     * current priority.
     */
    if (dlst_has_next(&g_cur_task->sched_item)) {
        yield_required = true;
    }
#endif

    if (yield_required) {
        invoke_scheduler();
    }
}
#endif /* !defined(KERNEL_NO_TIME) || defined(SCHED_USE_TIME_SLICING) */

/**
 * @brief PendSV exception handler.
 */
__attribute__((naked)) void PendSV_Handler(void)
{
    __asm volatile
    (
        "1:                          \n"
        "    movs r0, %[basepri]     \n"
        "    msr basepri, r0         \n" /* Enter critical */
#if defined(__CM7_REV) && __CM7_REV == 0x0001u
        "    ands r0, r0             \n" /* Workaround for Cortex-M7 r0p1 erratum n°837070 */
#endif
        "    bl %[choose_next_task]  \n" /* Choose next running task */
        "    cbnz r0, 2f             \n" /* Ensure at least one task is ready */
        "    ldr r0, =%[scr]         \n" /* If not, prepare to sleep */
        "    ldr r1, [r0]            \n"
        "    bic r1, r1, #0x04       \n" /* Ensure SLEEPDEEP bit is cleared */
        "    str r1, [r0]            \n"
        "    movs r2, #0             \n"
        "    msr basepri, r2         \n" /* Allow all exceptions to execute */
        "    wfi                     \n" /* Wait for interrupt */
        "    b 1b                    \n" /* Loop to check if a task is ready */
        "2:                          \n"
        "    movs r2, #0             \n"
        "    msr basepri, r2         \n" /* Exit critical */
        "    ldr r1, =%[g_cur_task]  \n" /* Get current task */
        "    ldr r2, [r1]            \n"
        "    mrs r3, psp             \n" /* Get the current task stack pointer */
#ifndef KERNEL_NO_FPU
        "    tst lr, #0x10           \n" /* Check if the task has used the FPU */
        "    it eq                   \n"
        "    vstmdbeq r3!, {s16-s31} \n" /* If so, save the FPU registers */
#endif
        "    stmdb r3!, {r4-r11, lr} \n" /* Save the core registers */
        "    str r3, [r2]            \n" /* Store in current TCB the new stack pointer value */
        "    ldr r3, [r0]            \n" /* Load from new TCB the stack pointer value */
        "    ldmia r3!, {r4-r11, lr} \n" /* Restore core registers */
#ifndef KERNEL_NO_FPU
        "    tst lr, #0x10           \n" /* Check if the task has used FPU */
        "    it eq                   \n"
        "    vldmiaeq r3!, {s16-s31} \n" /* If so, restore FPU registers */
#endif
        "    msr psp, r3             \n" /* Update the task stack pointer */
        "    str r0, [r1]            \n" /* Update g_cur_task */
        "    bx lr                   \n" /* Return to the new task */
        ::
        [basepri] "i" (KERNEL_CRITICAL_SEC_BASEPRI),
        [scr] "i" (&SCB->SCR),
        [g_cur_task] "i" (&g_cur_task),
        [choose_next_task] "i" (choose_next_task)
        : "memory"
    );
}

static void invoke_scheduler(void)
{
    __COMPILER_BARRIER();
    SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk;
}

static task_t* choose_next_task(void)
{
    ASSERT(kernel_is_in_critical_section());

    if (g_ready_priorities == 0u) {
        return NULL; /* No ready task available */
    }
    /* Choose task with highest priority */
    uint32_t highest_ready_prio = get_highest_ready_prio();
    dlst_t* ready_queue = &g_ready_queues[highest_ready_prio];
    task_t* next_task = task_from_sched_item(ready_queue->head);

#ifdef SCHED_USE_TIME_SLICING
    /* If using time slicing, move new active task to the end of the queue */
    dlst_rotate(&g_ready_queues[highest_ready_prio]);
#endif

    return next_task;
}

static uint32_t get_highest_ready_prio(void)
{
    return __builtin_ctz(g_ready_priorities);
}

static void register_ready_task(task_t* task)
{
    ASSERT(task != NULL);
    ASSERT(task->wait_queue == NULL);
    ASSERT(kernel_is_in_critical_section());

    g_ready_priorities |= 1u << task->priority;
    dlst_append(&g_ready_queues[task->priority], &task->sched_item);
}

static void unregister_ready_task(task_t* task)
{
    ASSERT(task != NULL);
    ASSERT(task->wait_queue == NULL);
    ASSERT(kernel_is_in_critical_section());

    dlst_t* ready_queue = &g_ready_queues[task->priority];

    dlst_remove(ready_queue, &task->sched_item);

    if (dlst_is_empty(ready_queue)) {
        g_ready_priorities &= ~(1u << task->priority);
    }
}

static void block_current_task(task_evt_t waited_event, uint32_t timeout)
{
    ASSERT(kernel_is_in_critical_section());

    if (UNLIKELY(timeout == 0u)) {
        return;
    }

    unregister_ready_task(g_cur_task);

    g_cur_task->waited_event = waited_event;

#ifdef KERNEL_NO_TIME
    ASSERT(timeout == KERNEL_WAIT_FOREVER);
#else
    if (timeout != KERNEL_WAIT_FOREVER) {
        uint32_t cur_tick = kernel_get_tick();
        uint32_t waking_tick = cur_tick + timeout;

        dlst_t* queue;

        if (waking_tick < cur_tick) {
            queue = g_overflow_timeout_queue;
        } else {
            queue = g_timeout_queue;
        }

        add_to_timeout_queue(queue, g_cur_task, waking_tick);
    }
#endif

    invoke_scheduler();
}

static uint32_t block_current_task_until(task_evt_t waited_event, uint64_t waking_time)
{
    ASSERT(kernel_is_in_critical_section());

    uint32_t timeout;

#ifdef KERNEL_NO_TIME
    ASSERT(waking_time == 0u || waking_time == KERNEL_WAIT_FOREVER);

    timeout = (uint32_t) waking_time;
#else
    uint64_t cur_time = kernel_get_time();

    if (waking_time <= cur_time) {
        return ERR_TIMEOUT; /* Waking time already reached */
    }

    uint64_t diff = waking_time - cur_time;

    if (diff >= UINT32_MAX) {
        ASSERT(waking_time == UINT64_MAX);
        timeout = KERNEL_WAIT_FOREVER;
    } else {
        timeout = (uint32_t) diff;
    }
#endif

    block_current_task(waited_event, timeout);
    return 0u;
}

static void unblock_task(task_t* task)
{
    ASSERT(task != NULL);
    ASSERT(task->waited_event != TASK_EVT_NONE);
    ASSERT(kernel_is_in_critical_section());

    bool yield_required = unblock_task_no_yield(task);

    if (yield_required) {
        invoke_scheduler();
    }
}

static bool unblock_task_no_yield(task_t* task)
{
    ASSERT(task != NULL);
    ASSERT(task->waited_event != TASK_EVT_NONE);
    ASSERT(kernel_is_in_critical_section());

    bool yield_required = false;

    task->waited_event = TASK_EVT_NONE;

    if (task->wait_queue != NULL) {
        remove_task_from_wait_queue(task);
    }

#ifndef KERNEL_NO_TIME
    dlst_remove(task->owning_timeout_queue, &task->timeout_item);
#endif

    register_ready_task(task);

    yield_required = task_cmp_prio(task, g_cur_task) > 0;

    return yield_required;
}

static void try_unblock_task(task_t* task, task_evt_t event)
{
    ASSERT(task != NULL);
    ASSERT(kernel_is_in_critical_section());

    if (task->waited_event != event) {
        return;
    }

    unblock_task(task);
}

static void add_task_to_wait_queue(dlst_t* queue, task_t* task)
{
    ASSERT(queue != NULL);
    ASSERT(task != NULL);
    ASSERT(kernel_is_in_critical_section());

    dlst_item_t* task_item = &task->sched_item;
    task_t* first = task_from_sched_item(queue->head);

    if (dlst_is_empty(queue) || task_cmp_prio(task, first) > 0) {
        dlst_prepend(queue, task_item);
    } else {
        dlst_item_t* cur = dlst_get_tail(queue);

        while (task_cmp_prio(task_from_sched_item(cur), task) < 0) {
            cur = cur->prev;
        }

        dlst_insert(cur, task_item);
    }

    task->wait_queue = queue;
}

static void remove_task_from_wait_queue(task_t* task)
{
    ASSERT(task != NULL);
    ASSERT(task->wait_queue != NULL);
    ASSERT(kernel_is_in_critical_section());

    dlst_remove(task->wait_queue, &task->sched_item);
    task->wait_queue = NULL;
}

#ifndef KERNEL_NO_TIME
static void add_to_timeout_queue(dlst_t* queue, task_t* task, uint32_t waking_tick)
{
    ASSERT(queue != NULL);
    ASSERT(task != NULL);

    task->waking_tick = waking_tick;
    task->owning_timeout_queue = queue;

    dlst_item_t* task_item = &task->timeout_item;
    task_t* first = task_from_timeout_item(queue->head);

    if (dlst_is_empty(queue) || waking_tick <= first->waking_tick) {
        dlst_prepend(queue, task_item);
    } else {
        dlst_item_t* cur = dlst_get_tail(queue);

        while (task_from_timeout_item(cur)->waking_tick > waking_tick) {
            cur = cur->prev;
        }

        dlst_insert(cur, task_item);
    }
}
#endif
