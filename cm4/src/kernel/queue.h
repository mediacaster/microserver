/**
 * @file
 * @brief Thread-safe blocking queue.
 * @author Thomas Altenbach
 */

#ifndef KERNEL_QUEUE_H_INTERNAL
#define KERNEL_QUEUE_H_INTERNAL

#include <kernel/queue.h>

#endif /* KERNEL_QUEUE_H_INTERNAL */
