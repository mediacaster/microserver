/**
 * @file
 * @brief Kernel task.
 * @author Thomas Altenbach
 *
 * The routines declared in this file are internal kernel routines and must
 * *not* be called from the application code.
 */

#ifndef KERNEL_TASK_H_INTERNAL
#define KERNEL_TASK_H_INTERNAL

#include <kernel/task.h>

/**
 * @enum
 * @brief Events on which a task can wait.
 */
typedef enum {
    TASK_EVT_NONE = 0,
    TASK_EVT_SLEEP_END,
    TASK_EVT_NOTIF,
    TASK_EVT_USER
} task_evt_t;

/**
 * @brief Initializes a new task control block.
 * @param task The buffer to use to store the task control block.
 * @param func The task routine.
 * @param priority The task priority, lower numeric value means higher priority.
 * @param stack_buf The buffer to use as stack for the task.
 * @param stack_size The size of the task stack, in words.
 *
 * Note that this routine does not register the task with the scheduler, only
 * the task control block is initialized.
 *
 * This routine can safely be called from an exception handler.
 */
void task_init(task_t* task, task_func_t func, uint8_t priority, uint32_t* stack_buf,
               size_t stack_size);

/**
 * @brief Compares two task priority values.
 * @param p1 The first priority value.
 * @param p2 The second priority value.
 * @return > 0 if p1 represents a higher priority than p2,
 *         < 0 if p2 represents a higher priority than p1,
 *           0 if both priority values are equals.
 */
__STATIC_FORCEINLINE int32_t task_cmp_prio_val(uint8_t p1, uint8_t p2)
{
    /* A lower numeric value means a higher priority */
    return p2 - p1;
}

/**
 * @brief Indicates which of two tasks has the highest priority.
 * @param t1 The first task.
 * @param t2 The second task.
 * @return > 0 if t1 has a higher priority than t2,
 *         < 0 if t2 has a higher priority than t1,
 *           0 if both tasks have the same priority.
 */
__STATIC_FORCEINLINE int32_t task_cmp_prio(task_t const* t1, task_t const* t2)
{
    ASSERT(t1 != NULL);
    ASSERT(t2 != NULL);

    return task_cmp_prio_val(t1->priority, t2->priority);
}

/**
 * @brief Gets the task associated with a given ready or wait queue item.
 * @param item An item of a ready or wait queue.
 * @return The task associated with the specified item.
 */
__STATIC_INLINE task_t* task_from_sched_item(dlst_item_t const* item)
{
    ASSERT(item != NULL);
    return CONTAINER_OF(item, task_t, sched_item);
}

#ifndef KERNEL_NO_TIME
/**
 * @brief Gets the task associated with a given timeout queue item.
 * @param item An item of a timeout queue.
 * @return The task associated with the specified item.
 */
__STATIC_INLINE task_t* task_from_timeout_item(dlst_item_t const* item)
{
    ASSERT(item != NULL);
    return CONTAINER_OF(item, task_t, timeout_item);
}
#endif /* !KERNEL_NO_TIME */

#endif /* KERNEL_TASK_H_INTERNAL */
