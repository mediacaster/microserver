/**
 * @file
 * @brief Thread-safe blocking queue.
 * @author Thomas Altenbach
 */

#include "queue.h"
#include "kernel.h"
#include "sched.h"
#include "sem.h"

static void copy_item(uint32_t* dest, uint32_t const* src, size_t size);

static uint32_t fqueue_move_item(fqueue_t* queue, uint32_t* dest, uint32_t const* src,
                                 size_t blocking_free_items, size_t* cur_buf_ptr, uint32_t timeout);

void queue_init(queue_t* queue, uint32_t* buffer, size_t item_size, size_t len)
{
    ASSERT(queue != NULL);
    ASSERT(buffer != NULL);
    ASSERT(IS_POW2(len));

    queue->buffer = buffer;
    queue->head = 0u;
    queue->tail = 0u;
    queue->len = len;
    queue->item_size = item_size;

    sem_init(&queue->poll_sem, 0u);
    sem_init(&queue->offer_sem, len);
}

uint32_t queue_poll(queue_t* queue, uint32_t* dest, uint32_t timeout)
{
    ASSERT(queue != NULL);
    ASSERT(dest != NULL);
    ASSERT(timeout == 0u || sched_can_switch_context());

    uint32_t ret = sem_take(&queue->poll_sem, timeout);

    if (ret != 0u) {
        return ret;
    }

    kernel_enter_critical();

    copy_item(dest, &queue->buffer[queue->head], queue->item_size);
    queue->head = (queue->head + 1u) & ~queue->len;

    sem_give(&queue->poll_sem);

    kernel_exit_critical();

    return 0;
}

uint32_t queue_offer(queue_t* queue, uint32_t const* item, uint32_t timeout)
{
    ASSERT(queue != NULL);
    ASSERT(item != NULL);

    uint32_t ret = sem_take(&queue->offer_sem, timeout);

    if (ret != 0u) {
        return ret;
    }

    kernel_enter_critical();

    copy_item(&queue->buffer[queue->tail], item, queue->item_size);
    queue->tail = (queue->tail + 1u) & ~queue->len;

    sem_give(&queue->poll_sem);

    kernel_exit_critical();

    return 0u;
}

void fqueue_init(fqueue_t* queue, uint32_t* buffer, size_t item_size, size_t len)
{
    ASSERT(queue != NULL);
    ASSERT(buffer != NULL);
    ASSERT(len != 0u && ((len & (len - 1u)) == 0u)); /* Ensure len is a power of two */

    queue->buffer = buffer;
    queue->head = 0u;
    queue->tail = 0u;
    queue->free_items = len;
    queue->len = len;
    queue->item_size = item_size;
    queue->waiting_task = NULL;
}

uint32_t fqueue_poll(fqueue_t* queue, uint32_t* dest, uint32_t timeout)
{
    ASSERT(queue != NULL);
    ASSERT(dest != NULL);
    ASSERT(timeout == 0u || sched_can_switch_context());

    return fqueue_move_item(queue, dest, &queue->buffer[queue->head], queue->len, &queue->head,
                            timeout);
}

uint32_t fqueue_offer(fqueue_t* queue, uint32_t const* item, uint32_t timeout)
{
    ASSERT(queue != NULL);
    ASSERT(item != NULL);
    ASSERT(timeout == 0u || sched_can_switch_context());

    return fqueue_move_item(queue, &queue->buffer[queue->tail], item, 0u, &queue->tail, timeout);
}

static void copy_item(uint32_t* dest, uint32_t const* src, size_t size)
{
    for (size_t i = 0u; i < size; ++i) {
        *dest++ = *src++;
    }
}

static uint32_t fqueue_move_item(fqueue_t* queue, uint32_t* dest, uint32_t const* src,
                                 size_t blocking_free_items, size_t* cur_buf_ptr, uint32_t timeout)
{
    if (queue->free_items == blocking_free_items) {
        if (timeout == 0u) {
            return ERR_TIMEOUT;
        }

        task_t* cur_task = sched_get_cur_task();

        kernel_enter_critical_no_nesting();

        if (LIKELY(queue->free_items != blocking_free_items)) {
            queue->waiting_task = cur_task;
            sched_wait(timeout);
        }

        kernel_exit_critical_no_nesting();

        if (queue->free_items == blocking_free_items) {
            return ERR_TIMEOUT;
        }
    }

    copy_item(dest, src, queue->item_size);
    *cur_buf_ptr = (*cur_buf_ptr + 1u) & ~queue->len;

    kernel_enter_critical();

    if (queue->waiting_task != NULL) {
        sched_notify(queue->waiting_task);
        queue->waiting_task = NULL;
    }

    kernel_exit_critical();

    return 0u;
}
