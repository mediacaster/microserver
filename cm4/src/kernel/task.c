/**
 * @file
 * @brief Kernel task.
 * @author Thomas Altenbach
 */

#include <stddef.h>
#include <stdint.h>
#include "task.h"

/** Initial task EXC_RETURN value (Thread mode, no FPU state saved, PSP) */
#define INIT_EXC_RETURN EXC_RETURN_THREAD_PSP
/** Initial task xPSR value (Thumb mode) */
#define INIT_XPSR xPSR_T_Msk

/** Minimum stack size, in words */
#define MIN_STACK_SIZE 17u /* R0->R12, PC, LR, xPSR, EXC_RETURN */

/**
 * @brief Initializes the stack of a new task.
 * @param stack The buffer that must be used as stack.
 * @param func The task routine.
 */
static void init_task_stack(uint32_t* stack, task_func_t func);

/**
 * @brief Routine called when a task returns.
 *
 * This indicates a programming error and should never happen in practice.
 */
static void on_task_exit(void);

void task_init(task_t* task, task_func_t func, uint8_t priority, uint32_t* stack_buf,
               size_t stack_size)
{
    ASSERT(task != NULL);
    ASSERT(func != NULL);
    ASSERT(priority < SCHED_PRIO_COUNT);
    ASSERT(stack_buf != NULL);
    ASSERT(stack_size >= MIN_STACK_SIZE);

    task->sp = &stack_buf[stack_size];
    task->func = func;
    task->priority = priority;
    task->base_priority = priority;
    task->event_value = 0u;
    task->waited_event = TASK_EVT_NONE;

    init_task_stack(task->sp, func);
}

static void init_task_stack(uint32_t* stack, task_func_t func)
{
    *(--stack) = INIT_XPSR;               /* xPSR */
    *(--stack) = (uint32_t) func;         /* PC */
    *(--stack) = (uint32_t) on_task_exit; /* LR */
    stack -= 13u;                         /* R12, R3->R0, R11->R4 */
    *(--stack) = INIT_EXC_RETURN;         /* EXC_RETURN */
}

static void on_task_exit(void)
{
    /*
     * A task must not return so that should never happen.
     * Disable all exceptions and wait forever to stop the program execution.
     */

    __disable_irq();

    while (true) {
        __asm volatile ("");
    }
}
