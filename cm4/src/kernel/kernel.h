/**
 * @file
 * @brief Kernel common definitions.
 * @author Thomas Altenbach
 *
 * The routines declared in this file are internal kernel routines and must
 * *not* be called from the application code.
 */

#ifndef KERNEL_H_INTERNAL
#define KERNEL_H_INTERNAL

#include <stdbool.h>
#include <kernel/kernel.h>

/** Value to be written in BASEPRI when entering a critical section */
#define KERNEL_CRITICAL_SEC_BASEPRI \
    (KERNEL_MAX_SYSCALL_IRQ_PRIO << (8u - __NVIC_PRIO_BITS))

/**
 * @brief Returns the time point at which a given timeout will expire.
 * @param timeout The timeout, in ticks.
 * @return The time point, in ticks, at which the specified timeout will expire
 *         if started when this function is called. If the timeout is infinite,
 *         UINT64_MAX is returned.
 *
 * This routine is defined even if kernel time management is disabled to avoid
 * the need of #ifdef in every calling routines. In that case the specified
 * timeout is simply returned and this routine will therefore return a zero
 * time value if no blocking is requested, and a non-zero value if infinite
 * timeout is given.
 */
#ifdef KERNEL_NO_TIME
__STATIC_FORCEINLINE uint64_t kernel_get_timeout_end(uint32_t timeout)
{
    ASSERT(timeout == 0u || timeout == KERNEL_WAIT_FOREVER);
    return timeout;
}
#else
uint64_t kernel_get_timeout_end(uint32_t timeout);
#endif

/**
 * @brief Indicates if this routine is executed in a critical section or not.
 * @return true if this routine was executed in a critical section,
 *         false otherwise.
 */
bool kernel_is_in_critical_section(void);

/**
 * @brief Indicates if this routine is executed in a nested critical section.
 * @return true if this routine was executed in a nested critical section,
 *         false otherwise.
 */
bool kernel_is_in_nested_critical_section(void);

/**
 * @brief Indicates if this routine is executed in a non-nested critical section.
 * @return true if this routine was executed in a non-nested critical section,
 *         false otherwise.
 */
bool kernel_is_in_non_nested_critical_section(void);

__STATIC_FORCEINLINE void kernel_enter_critical_no_nesting(void)
{
    ASSERT(!kernel_is_in_nested_critical_section());

    __COMPILER_BARRIER();

    /* Mask all interrupts that can use the kernel API */
    __set_BASEPRI(KERNEL_CRITICAL_SEC_BASEPRI);

#if defined(__CM7_REV) && __CM7_REV == 0x0001u
    /* Workaround for Cortex-M7 r0p1 erratum n°837070 */
    __asm volatile ("ands r0, r0");
#endif
}

__STATIC_FORCEINLINE void kernel_exit_critical_no_nesting(void)
{
    ASSERT(kernel_is_in_non_nested_critical_section());

    __COMPILER_BARRIER();
    __set_BASEPRI(0u);

    /* Ensure any masked pending exception will be executed immediately */
    __ISB();
}

#endif /* KERNEL_H_INTERNAL */
