#include <stm32h7xx.h>
#include "sysnotif.h"
#include "rcc.h"

#ifdef CORE_CM7
#define HSEM_IER         (HSEM->C1IER)
#define HSEM_ICR         (HSEM->C1ICR)
#define HSEM_MISR        (HSEM->C1MISR)
#define HSEM_IRQ_NUM     HSEM1_IRQn
#define HSEM_IRQ_HANDLER HSEM1_IRQHandler

#define EXTI_HSEM_EMR        (EXTI_D1->EMR3)
#define EXTI_HSEM_IMR        (EXTI_D1->IMR3)
#define EXTI_HSEM_EVENT_MASK EXTI_EMR3_EM77
#else
#define HSEM_IER         (HSEM->C2IER)
#define HSEM_ICR         (HSEM->C2ICR)
#define HSEM_MISR        (HSEM->C2MISR)
#define HSEM_IRQ_NUM     HSEM2_IRQn
#define HSEM_IRQ_HANDLER HSEM2_IRQHandler

#define EXTI_HSEM_EMR        (EXTI_D2->EMR3)
#define EXTI_HSEM_IMR        (EXTI_D2->IMR3)
#define EXTI_HSEM_EVENT_MASK EXTI_EMR3_EM78

#define USE_KERNEL
#endif

#ifdef USE_KERNEL
#include <kernel/kernel.h>
#include <kernel/sched.h>

#define HSEM_IRQ_PRIO KERNEL_MAX_SYSCALL_IRQ_PRIO

static task_t* g_waiting_task;
#endif

void sysnotif_init(void)
{
    rcc_ahb4_enable_clock(RCC_AHB4ENR_HSEMEN);

    /* Ensure all HSEM interrupts are disabled */
    HSEM_IER = 0U;
    /* Ensure all HSEM interrupt flags are cleared */
    HSEM_ICR = 0xFFFFFFFFU;

#ifdef USE_KERNEL
    NVIC_SetPriority(HSEM_IRQ_NUM, HSEM_IRQ_PRIO);
    NVIC_EnableIRQ(HSEM_IRQ_NUM);
#endif /* USE_KERNEL */

    /* Enable HSEM events from other CPU in EXTI */
    EXTI_HSEM_EMR |= EXTI_HSEM_EVENT_MASK;
    /* Disable HSEM interrupt in EXTI */
    EXTI_HSEM_IMR &= ~EXTI_HSEM_EVENT_MASK;
}

void sysnotif_send(uint32_t notif_id)
{
    /* Lock then release semaphore to trigger notification event */
    HSEM->RLR[notif_id];
    HSEM->R[notif_id] = HSEM_CR_COREID_CURRENT;
}

uint32_t sysnotif_wait(uint32_t enabled_notifs)
{
#ifdef USE_KERNEL
    __COMPILER_BARRIER();

    uint32_t hsem_misr;

    do {
        /* Enable HSEM interrupts for enabled notifications */
        HSEM_IER = enabled_notifs;
        /* Get current task handle */
        g_waiting_task = sched_get_cur_task();
        /* Block current task until an event occur */
        sched_wait_event(NULL, KERNEL_WAIT_FOREVER);
        /* Get the pending HSEM interrupts */
        hsem_misr = HSEM_MISR;
    } while (hsem_misr == 0U); /* Loop until a notification has been received */

    /* Clear interrupt flag for received notification(s) */
    HSEM_ICR = hsem_misr;

    return hsem_misr;
#else
    return sysnotif_wait_no_kernel(enabled_notifs);
#endif /* USE_KERNEL */
}

uint32_t sysnotif_wait_no_kernel(uint32_t enabled_notifs)
{
#ifdef USE_KERNEL
    NVIC_DisableIRQ(HSEM_IRQ_NUM);
#endif /* USE_KERNEL */

    __COMPILER_BARRIER();

    uint32_t hsem_misr;

    do {
        /* Enable HSEM interrupts for enabled notifications */
        HSEM_IER = enabled_notifs;
        /* Sleep until an event occur */
        __WFE();
        /* Disable all HSEM interrupts, freezing MISR value */
        HSEM_IER = 0U;
        /* Get the pending HSEM interrupts */
        hsem_misr = HSEM_MISR;
    } while (hsem_misr == 0U); /* Loop until a notification has been received */

    /* Clear interrupt flag for received notification(s) */
    HSEM_ICR = hsem_misr;

#ifdef USE_KERNEL
    NVIC_ClearPendingIRQ(HSEM_IRQ_NUM);
    NVIC_EnableIRQ(HSEM_IRQ_NUM);
#endif /* USE_KERNEL */

    return hsem_misr;
}

void sysnotif_clear(uint32_t notifs)
{
    HSEM_ICR = notifs;
}

#ifdef USE_KERNEL
void HSEM_IRQ_HANDLER(void)
{
    /* Disable all HSEM interrupts */
    HSEM_IER = 0U;
    /* Wake-up task */
    sched_send_event(g_waiting_task, 0x01U);
}
#endif /* USE_KERNEL */
