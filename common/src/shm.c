#include <stdint.h>
#include "shm.h"
#include "asrt.h"

#define SHM_CAPACITY 1024U

__attribute__((section(".shm")))
static uint8_t g_shm_data[SHM_CAPACITY];

static uint32_t g_shm_head = 0;

void* shm_alloc(size_t size)
{
    ASSERT(size <= SHM_CAPACITY - g_shm_head);

    void* data = &g_shm_data[g_shm_head];
    g_shm_head += size;
    return data;
}
