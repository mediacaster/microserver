#include "player.h"
#include "sysnotif.h"
#include "shm.h"
#include "asrt.h"

#if defined(CORE_CM7)
#define THIS_CORE_UPDATE_NOTIF_ID    SYSNOTIF_CPU1_PLAYER_UPDATE_ID
#define OTHER_CORE_UPDATE_NOTIF_MASK SYSNOTIF_CPU2_PLAYER_UPDATE_MASK
#elif defined(CORE_CM4)
#define THIS_CORE_UPDATE_NOTIF_ID    SYSNOTIF_CPU2_PLAYER_UPDATE_ID
#define OTHER_CORE_UPDATE_NOTIF_MASK SYSNOTIF_CPU1_PLAYER_UPDATE_MASK
#else
#error "Please define either CORE_CM7 or CORE_CM4"
#endif /* CORE_CM7, CORE_CM4 */

static player_state_t volatile* g_state = NULL;

void player_init(void)
{
    /*
     * Ensure player_state_t is 64 bits and that its high and low words can be
     * accessed atomically.
     */
    ASSERT(sizeof(player_state_t) == 8U && ((uint32_t) &g_state & 0x03U) == 0U);
    /* Ensure not already initialized */
    ASSERT(g_state == NULL);

    g_state = shm_alloc(sizeof(player_state_t));

    /* Clear the shared state */
    g_state->block_raw = 0U;
    g_state->playback_raw = 0U;

    /* Set the volume to maximum value by default */
    g_state->volume = 100U;
}

void player_get_state(player_state_t* state)
{
    state->playback_raw = g_state->playback_raw;
    state->block_raw = g_state->block_raw;
}

void player_play(audiofmt_t format)
{
    player_state_t new_state;
    new_state.playback_raw = 0U;

    new_state.playback_state = PLAYER_PLAYBACK_PLAYING;
    new_state.format = format;
    new_state.volume = g_state->volume;

    g_state->block_raw = 0U;
    g_state->playback_raw = new_state.playback_raw;

    sysnotif_send(THIS_CORE_UPDATE_NOTIF_ID);
}

void player_pause(void)
{
    g_state->playback_state = PLAYER_PLAYBACK_PAUSED;
    sysnotif_send(THIS_CORE_UPDATE_NOTIF_ID);
}

void player_resume(void)
{
    g_state->playback_state = PLAYER_PLAYBACK_PLAYING;
    sysnotif_send(THIS_CORE_UPDATE_NOTIF_ID);
}

void player_set_eof(void)
{
    g_state->eof = 1U;
    sysnotif_send(THIS_CORE_UPDATE_NOTIF_ID);
}

void player_request_hard_stop(void)
{
    g_state->playback_state = PLAYER_PLAYBACK_STOPPING;
    sysnotif_send(THIS_CORE_UPDATE_NOTIF_ID);
}

void player_notify_stopped(void)
{
    g_state->playback_state = PLAYER_PLAYBACK_STOPPED;
    sysnotif_send(THIS_CORE_UPDATE_NOTIF_ID);
}

uint32_t player_is_stopped(void)
{
    return g_state->playback_state == PLAYER_PLAYBACK_STOPPED;
}

uint32_t player_is_eof(void)
{
    return g_state->eof;
}

void player_notify_block_ready(uint32_t block_ind, uint16_t block_size)
{
    ASSERT(block_ind < PLAYER_AUDIO_BUF_BLOCK_COUNT);

    g_state->block_sizes[block_ind] = block_size;
    sysnotif_send(THIS_CORE_UPDATE_NOTIF_ID); // TODO: CPU1 will be waken when MDMA transfer has completed so it may not be necessary to
                                              //       send a notif if it is ensured that this function is called before starting the DMA
                                              //       and CPU1 do not use this value to test if a block is ready but wait for MDMA IRQ.
                                              //       Block size could also be stored in the block, at the start (header), which would
                                              //       allow to store player_state_t on 32 bits.
}

void player_notify_block_processed(uint32_t block_ind)
{
    ASSERT(block_ind < PLAYER_AUDIO_BUF_BLOCK_COUNT);

    g_state->block_sizes[block_ind] = 0U;
    sysnotif_send(THIS_CORE_UPDATE_NOTIF_ID);
}

void player_set_volume(uint32_t volume)
{
    g_state->volume = volume;
    sysnotif_send(THIS_CORE_UPDATE_NOTIF_ID);
}
