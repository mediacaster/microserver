#include <stdbool.h>
#include <cmsis_compiler.h>
#include "asrt.h"

#ifdef DEBUG
void __attribute__((weak)) assert_failed(char const* file, uint32_t line)
{
    (void) file;
    (void) line;

    /* Never return */
    while (true) {
        __NOP();
    }
}
#endif /* NDEBUG */
