#ifndef UTIL_H
#define UTIL_H

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))

#define IS_POW2(x) (((x) != 0U) && ((x) & ((x) - 1U)) == 0U)

#define ALIGN2(addr) (((uint32_t) (addr) + 0x1U) & ~0x1U)
#define ALIGN4(addr) (((uint32_t) (addr) + 0x3U) & ~0x3U)

#endif /* UTIL_H */
