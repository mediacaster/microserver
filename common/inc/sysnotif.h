#ifndef SYSNOTIF_H
#define SYSNOTIF_H

#include <stdint.h>

#define SYSNOTIF_MASK(id) (1U << (id))

#define SYSNOTIF_SYSTEM_READY_ID       0U
#define SYSNOTIF_CPU1_PLAYER_UPDATE_ID 0U
#define SYSNOTIF_CPU2_PLAYER_UPDATE_ID 31U

#define SYSNOTIF_SYSTEM_READY_MASK       SYSNOTIF_MASK(SYSNOTIF_SYSTEM_READY_ID)
#define SYSNOTIF_CPU1_PLAYER_UPDATE_MASK SYSNOTIF_MASK(SYSNOTIF_CPU1_PLAYER_UPDATE_ID)
#define SYSNOTIF_CPU2_PLAYER_UPDATE_MASK SYSNOTIF_MASK(SYSNOTIF_CPU2_PLAYER_UPDATE_ID)

void sysnotif_init(void);

void sysnotif_send(uint32_t notif_id);

uint32_t sysnotif_wait(uint32_t enabled_notifs);

uint32_t sysnotif_wait_no_kernel(uint32_t enabled_notifs);

void sysnotif_clear(uint32_t notifs);

#endif /* SYSNOTIF_H */
