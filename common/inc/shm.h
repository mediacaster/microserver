#ifndef SHM_H
#define SHM_H

#include <stddef.h>

void* shm_alloc(size_t size);

#endif /* SHM_H */
