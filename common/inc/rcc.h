#ifndef RCC_H
#define RCC_H

#include <stdint.h>
#include <stm32h7xx.h>

__STATIC_FORCEINLINE void rcc_enable_clock(uint32_t volatile* rcc_enr, uint32_t periph)
{
    *rcc_enr |= periph;

    /* Take into account delay after RCC peripheral clock enabling */
    uint32_t tmp = *rcc_enr;
    (void) tmp;
}

__STATIC_FORCEINLINE void rcc_disable_clock(uint32_t volatile* rcc_enr, uint32_t periph)
{
    *rcc_enr &= ~periph;
}

__STATIC_FORCEINLINE void rcc_ahb1_enable_clock(uint32_t periph)
{
    rcc_enable_clock(&RCC->AHB1ENR, periph);
}

__STATIC_FORCEINLINE void rcc_ahb2_enable_clock(uint32_t periph)
{
    rcc_enable_clock(&RCC->AHB2ENR, periph);
}

__STATIC_FORCEINLINE void rcc_ahb3_enable_clock(uint32_t periph)
{
    rcc_enable_clock(&RCC->AHB3ENR, periph);
}

__STATIC_FORCEINLINE void rcc_ahb4_enable_clock(uint32_t periph)
{
    rcc_enable_clock(&RCC->AHB4ENR, periph);
}

__STATIC_FORCEINLINE void rcc_apb1l_enable_clock(uint32_t periph)
{
    rcc_enable_clock(&RCC->APB1LENR, periph);
}

__STATIC_FORCEINLINE void rcc_apb1h_enable_clock(uint32_t periph)
{
    rcc_enable_clock(&RCC->APB1HENR, periph);
}

__STATIC_FORCEINLINE void rcc_apb2_enable_clock(uint32_t periph)
{
    rcc_enable_clock(&RCC->APB2ENR, periph);
}

__STATIC_FORCEINLINE void rcc_apb3_enable_clock(uint32_t periph)
{
    rcc_enable_clock(&RCC->APB3ENR, periph);
}

__STATIC_FORCEINLINE void rcc_apb4_enable_clock(uint32_t periph)
{
    rcc_enable_clock(&RCC->APB4ENR, periph);
}

__STATIC_FORCEINLINE void rcc_ahb1_disable_clock(uint32_t periph)
{
    rcc_disable_clock(&RCC->AHB1ENR, periph);
}

__STATIC_FORCEINLINE void rcc_ahb2_disable_clock(uint32_t periph)
{
    rcc_disable_clock(&RCC->AHB2ENR, periph);
}

__STATIC_FORCEINLINE void rcc_ahb3_disable_clock(uint32_t periph)
{
    rcc_disable_clock(&RCC->AHB3ENR, periph);
}

__STATIC_FORCEINLINE void rcc_ahb4_disable_clock(uint32_t periph)
{
    rcc_disable_clock(&RCC->AHB4ENR, periph);
}

__STATIC_FORCEINLINE void rcc_apb1l_disable_clock(uint32_t periph)
{
    rcc_disable_clock(&RCC->APB1LENR, periph);
}

__STATIC_FORCEINLINE void rcc_apb1h_disable_clock(uint32_t periph)
{
    rcc_disable_clock(&RCC->APB1HENR, periph);
}

__STATIC_FORCEINLINE void rcc_apb2_disable_clock(uint32_t periph)
{
    rcc_disable_clock(&RCC->APB2ENR, periph);
}

__STATIC_FORCEINLINE void rcc_apb3_disable_clock(uint32_t periph)
{
    rcc_disable_clock(&RCC->APB3ENR, periph);
}

__STATIC_FORCEINLINE void rcc_apb4_disable_clock(uint32_t periph)
{
    rcc_disable_clock(&RCC->APB4ENR, periph);
}

#endif /* RCC_H */
