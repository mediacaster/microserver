#ifndef ASRT_H
#define ASRT_H

#include <stdint.h>

#ifdef DEBUG
#define ASSERT(x) ((x) ? (void)0 : assert_failed(__FILE__, __LINE__))

void assert_failed(char const* file, uint32_t line);
#else /* NDEBUG */
#define ASSERT(x) ((void) 0)
#endif /* NDEBUG */

#endif /* ASRT_H */
