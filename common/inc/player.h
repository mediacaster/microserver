#ifndef PLAYER_H
#define PLAYER_H

#include <stdint.h>
#include "audiofmt.h"

#define PLAYER_AUDIO_BUF_ADDR        ((void*) 0x20000000U)
#define PLAYER_AUDIO_BUF_BLOCK_SIZE  (32U * 1024U)
#define PLAYER_AUDIO_BUF_BLOCK_COUNT 2U
#define PLAYER_AUDIO_BUF_SIZE        (PLAYER_AUDIO_BUF_BLOCK_SIZE * \
                                      PLAYER_AUDIO_BUF_BLOCK_COUNT)

typedef enum {
    PLAYER_PLAYBACK_STOPPED  = 0,
    PLAYER_PLAYBACK_STOPPING = 1,
    PLAYER_PLAYBACK_PAUSED   = 2,
    PLAYER_PLAYBACK_PLAYING  = 3
} player_playback_state_t;

typedef union {
    struct {
        uint16_t block_sizes[PLAYER_AUDIO_BUF_BLOCK_COUNT];
        uint8_t playback_state;
        uint8_t eof;
        uint8_t format;
        uint8_t volume;
    };
    struct {
        uint32_t block_raw;
        uint32_t playback_raw;
    };
} player_state_t;

void player_init(void);

void player_get_state(player_state_t* state);

void player_play(audiofmt_t format);

void player_pause(void);

void player_resume(void);

void player_set_eof(void);

void player_request_hard_stop(void);

uint32_t player_is_stopped(void);

uint32_t player_is_eof(void);

void player_notify_stopped(void);

void player_notify_block_ready(uint32_t block_ind, uint16_t block_size);

void player_notify_block_processed(uint32_t block_ind);

void player_set_volume(uint32_t volume);

#endif /* PLAYER_H */
