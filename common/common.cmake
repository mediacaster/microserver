#
# @file
# @brief CMake file for Cortex-M4 and Cortex-M7 firmware common code
# @author Thomas Altenbach
#

target_link_libraries(${EXECUTABLE} PRIVATE cmsis)

file(GLOB_RECURSE COMMON_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/src/*.c
    ${CMAKE_CURRENT_LIST_DIR}/src/*.s
)

target_sources(${EXECUTABLE} PRIVATE ${COMMON_SOURCES})

target_include_directories(${EXECUTABLE} PRIVATE ${CMAKE_CURRENT_LIST_DIR}/inc)
